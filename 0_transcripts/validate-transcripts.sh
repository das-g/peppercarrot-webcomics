#!/bin/bash
#
#  validate-transcripts.sh
#
#  SPDX-License-Identifier: GPL-3.0-or-later
#  SPDX-FileCopyrightText: © 2019 GunChleoc <fios@foramnagaidhlig.net>
#  SPDX-FileCopyrightText: © 2021 David Revoy <info@davidrevoy.com>
#
#  Script for validating Markdown transcript contents and their generation
#  Depency required: parallel

# Color palette for the output
export Off=$'\e[0m'
export Green=$'\e[1;32m'
export Red=$'\e[1;31m'
export Yellow=$'\e[1;33m'
export Blue=$'\e[1;34m'

export scriptdir="0_transcripts"

# Move up if we're not in the base directory.
if [ -d "../$scriptdir" ]; then
    pushd ..
fi

# Run unit tests

0_transcripts/run_tests.py
if [[ ! $? -eq 0 ]] ; then
    echo " "
    echo "ERROR: Unit tests failed"
    exit 1
fi

# Run integration tests
declare -i errors; errors=0

# Find episodes/locales that have transcripts and validate them
function_validate_transcript() { 
  episodedir=$1
  echo "${Blue}=> $episodedir ${Off}"
  # We have a directory. Search for any locales
  for localedir in ${episodedir}/lang/*; do
      if [ -d "${localedir}" ] ; then
          # Only process directories that have transcripts in them
          mdfile_found=0
          locale="en"
          for mdfile in ${localedir}/ep*_transcript.md; do
              # Get the locale from the file's path
              locale=$(basename $(dirname $mdfile))

              if [ -f "$mdfile" ] ; then
                  # There is a transcript for this episode and locale.
                  # Validate it and then break to the next locale.
                  mdfile_found=1
                  mdfilename=$(basename ${mdfile})

                  # Check if the transcript filename is well formed 
                  # (eg. in ep01_Potion-of-Flight/lang/fr it should be ep01_fr_transcript.md)
                  epXX=${episodedir:0:4}
                  transcriptexpectedname="${epXX}_${locale}_transcript.md"
                  # Debug

                  if [ ${mdfilename} != ${transcriptexpectedname} ];then
                      mv ${episodedir}/lang/${locale}/${mdfilename} ${episodedir}/lang/${locale}/${transcriptexpectedname}
                      # If renaming was a success, print a notification then mutate also the variables in the loop to adapt to the new filename.
                      if [ -f ${episodedir}/lang/${locale}/${transcriptexpectedname} ] ; then
                        echo " ${Red}==> ${mdfilename}${Off} --> ${Red}${transcriptexpectedname} ${Off} (wrong filename, renamed)."
                        mdfilename=${transcriptexpectedname}
                        mdfile="${episodedir}/lang/${locale}/${transcriptexpectedname}"
                      fi
                  fi

                  # Create a directory inside the cache if missing
                  if ! [ -d "${episodedir}/cache/${locale}" ];then
                    mkdir -p "${episodedir}/cache/${locale}"
                  fi

                  # Regenerate transcript to cache to check if it will run through
                  logoutput=$($scriptdir/extract_text.py $episodedir $locale ${episodedir}/cache/${locale})
                  # Output is noisy, so storing it in a variable to be printed on error only
                  if [[ ! $? -eq 0 ]] ; then
                      echo " "
                      echo "${Red}[ERROR]${Off} Updating transcript encountered an error for $episodedir $locale "
                      for line in "${logoutput[@]}" ; do
                        echo "$line"
                      done
                      errors+=1
                      echo " "
                  fi

                  # Compare the extracted fresh (cached) transcript with the official transcript in usage
                  if ! diff ${episodedir}/cache/${locale}/${mdfilename} ${mdfile} &>/dev/null ; then
                    # If our version contains update or is new, it's safe to update the file and export htmls
                    echo " ${Green}==> ${mdfilename} new or modified: update file and extract html and pad. ${Off}"
                    cp ${episodedir}/cache/${locale}/${mdfilename} ${mdfile}

                    # Generate HTML to check if it will run through
                    # Output is noisy, so storing it in a variable to be printed on error only
                    logoutput=$($scriptdir/extract_to_html.py $episodedir $locale)
                    if [[ ! $? -eq 0 ]] ; then
                      echo " "
                      echo "${Red}[ERROR]${Off} Generating HTML encountered an error for $episodedir $locale"
                      for line in "${logoutput[@]}" ; do
                        echo "$line"
                      done
                      errors+=1
                      echo " "
                    fi

                    # Generate the Pad
                    # Create a directory to store them
                    if ! [ -d "${episodedir}/hi-res/pad" ];then
                      mkdir -p "${episodedir}/hi-res/pad"
                    fi
                    $scriptdir/convert-transcript-to-framapad.sh ${mdfile} "${episodedir}/hi-res/pad"
                    # Note: to regenerate all Pad; just move out this paragraph from the if/fi condition under.

                  fi

                  # Here is a good entry door to do batch work on all episode folder, in a multithread way, eg. :
                  # rm ${episodedir}/hi-res/pad/*_pad.txt

              break
              fi
          done
          # If we didn't regenerate the markdown, we still check if the svg parses for future transcripts
          if [ $mdfile_found -eq 0 ] ; then
              $scriptdir/check_xml.py $episodedir $locale
              if [[ ! $? -eq 0 ]] ; then
                  echo "${Red}[ERROR]${Off} Found invalid SVG for $episodedir $locale"
                  errors+=1
              fi
          fi
      fi
  done
}

# Execute function_validate_transcript on parallel threads.
function_parallel_validate_transcript() { 
  echo ""
  echo "${Yellow} [VALIDATE SVG AND TRANSCRIPT] ${Off}"
  echo "${Yellow} =-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ${Off}"

  export -f function_validate_transcript
  ls -1d */ | parallel function_validate_transcript "{}"
}

# Start
function_parallel_validate_transcript


if [[ (${errors} > 0 )]] ; then
    echo "#####################"
    echo "  Found ${errors} error(s)"
    echo "#####################"
    exit 1
fi

echo "${Blue}Done. ${Off}"

# Show the status of what changed
echo ""
echo "${Yellow} [GIT STATUS] ${Off}"
echo "${Yellow} =-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ${Off}"
git status
