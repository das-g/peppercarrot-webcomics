#. ~~~~~~ NOTICE: The primary purpose of this file is to help your translations
#. be consistent across episodes, and as a guide when multiple translators work
#. on the same language. secondary for generating transcripts. Comments
#. starting by #. are coming from _catalog.pot file, will always overwritten if
#. you edit them. To write own comments, please start them simply with # (no
#. period) they'll stay around. You can read more about PO reference files in
#. translation documentation repository.
msgid ""
msgstr ""
"Language: עברית\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Translator: Name <email@adress.com>\n"

#. ~~~~~~ TRANSCRIPT SPECIFICS: Title of the episode
msgid "Title"
msgstr "כותרת"

#. Used for the sound effects ('SFX', onomatopoeia)
msgid "Sound"
msgstr "צליל"

#. For anything written on a wall, sign, paper, etc.
msgid "Writing"
msgstr "כיתוב"

#. For speech bubbles of the narrator
msgid "Narrator"
msgstr "מספר"

#. For notes appearing under panels
msgid "Note"
msgstr "פתק"

#. For the credits at end of each episode.
msgid "Credits"
msgstr "קרדיט"

#. ~~~~~~ CHARACTERS (Episode 1)
msgid "Pepper"
msgstr "פפר"

#. How the 'Pepper' & 'Carrot' words are connected in title -- for reference
#. only, not used by transcripts.
msgid "&"
msgstr "ו-"

msgid "Carrot"
msgstr "גזר"

#. (Episode 3, Episode 37) A male vendor at the market.
msgid "Vendor"
msgstr "מוכר"

#. A red-haired witch wearing luxurious jewels.
msgid "(Miss) Saffron"
msgstr "(מיס) סאפרון"

#. Saffron's pet, a white female cat.
msgid "Truffel"
msgstr "כמהין"

#. (Episode 6) A witch with curly black hair who is a princess and later queen.
msgid "Coriander"
msgstr "קוריאנדר"

#. Coriander's pet, a male black cockerel.
msgid "Mango"
msgstr "מנגו"

#. A witch: blonde, with pink- or red-irised eyes, in a kimono.
msgid "Shichimi"
msgstr "שיצימי"

#. Shichimi's pet, a male fox with multiple tails.
msgid "Yuzu"
msgstr "יוזו"

#. Also known as Mayor Bramble, he is a man with 'van Dyke' beard, brown suit
#. and hat; presents the potion magic contests.
msgid "Mayor of Komona"
msgstr "ראש-עיר קומונה"

#. Generic term for audiences.
msgid "Audience"
msgstr "קהל"

#. Generic term for any bird.
msgid "Bird"
msgstr "ציפור"

#. (Episode 7) Generic term for any fairy.
msgid "Fairies"
msgstr "פיות"

#. (Episode 8) Generic term for any monster.
msgid "Monster"
msgstr "מפלצת"

#. (Episode 11) The bon-vivant or happy-go-lucky witch who is one of Pepper's
#. three godmothers.
msgid "Cumin"
msgstr "כמון"

#. The tall, thin, and behaviourally-rigid witch who is one of Pepper's three
#. godmothers.
msgid "Cayenne"
msgstr "קאיין"

#. The ancient, small, and wise leader witch who is one of Pepper's three
#. godmothers.
msgid "Thyme"
msgstr "טימין"

#. Name of the young blond haired prince (as-yet-uncrowned king) Acren.
msgid "(Prince) Acren"
msgstr "אקרן"

#. Generic term used by Pepper to refer the three witches who raised her.
msgid "godmothers"
msgstr "סנדקיות"

#. (Episode 18) Master and teacher of Hippiah, an elf with blond curly hair.
msgid "Basilic"
msgstr "בזיל"

#. A student witch of Hippiah, with long dark hair.
msgid "Oregano"
msgstr "אורגנו"

#. A student witch of Hippiah, with short blond hair and freckles.
msgid "Cardamom"
msgstr "קרדמון"

#. A student witch of Hippiah and also an elf, with red hair.
msgid "Cinnamon"
msgstr "קינמון"

#. The main recurrent witch of Hippiah, a human with raccoon ears and tail.
msgid "Camomile"
msgstr "קמומיל"

#. (Episode 21) A witch of Aquah, living in water, with a long white hair
#. crest.
msgid "Spirulina"
msgstr "ספירולינה"

#. Spirulina's pet, a male _Betta splendens_ fish.
msgid "Durian"
msgstr "דוריאן"

#. (Episode 22) One of the jury members, he wears a white beard, monocle, and
#. wide-brimmed hat. His name references Frieza from Dragon Ball.
msgid "Lord Azeirf"
msgstr "לורד אזיירף"

#. One of the jury members, she is a queen. Her name and hairstyle reference
#. Leia Organa from Star Wars.
msgid "Queen Aiel"
msgstr "המלכה איהל"

#. (Episode 23) The big and muscular golden walrus genie.
msgid "Genie of Success"
msgstr "ג’יני ההצלחה"

#. (Episode 27) The royal tailor of Coriander.
msgid "Tailor"
msgstr "חייט"

#. The robotic invention of Coriander.
msgid "Psychologist-Bot"
msgstr "פסיכולובוט"

#. (Episode 28) Generic term for any/all of the journalists asking questions at
#. coronation party.
msgid "Journalist"
msgstr "עיתונאי"

#. (Episode 32) A bearded king wearing golden armour.
msgid "King"
msgstr "מלך"

#. The officer of the bearded king, with a long blond mustache.
msgid "Officer"
msgstr "שוטר"

#. Generic label used for all sounds produced by the armies.
msgid "Army"
msgstr "צבא"

#. (Episode 33) The name of the king with a hunting horn, leading opposing
#. ('dark') army
msgid "Enemy"
msgstr "אויב"

#. (Episode 34) A witch and knight of Ah, an elf from a desert. She is also the
#. teacher Shichimi.
msgid "Hibiscus"
msgstr ""

#. The supreme leader witch of Ah, with green hair.
msgid "Wasabi"
msgstr ""

#. (Episode 35) A witch of Ah who pilots a dragon, with spiky haircut and
#. aviator goggles. She is Shichimi's girlfriend.
msgid "Torreya"
msgstr ""

#. Torreya's pet, a white dragon.
msgid "Arra"
msgstr ""

#. (Episode 36) A rat appearing in the prison.
msgid "Rat"
msgstr ""

#. Generic terms for guards.
msgid "Guard"
msgstr ""

#. (Episode 37) The giant Phoenix
msgid "Phoenix"
msgstr ""

#. ~~~~~~ PLACES: (Episode 3) The flying city with the giant tree of Komona at
#. centre.
msgid "Komona City"
msgstr "העיר קומונה"

#. (Episode 6) The name of the village near Pepper's house.
msgid "Squirrel's End"
msgstr "חורשת הסנאי"

#. A region of the world (Hereva) that federates many cities including ruled by
#. Coriander, Qualicity.
msgid "Technologist's Union"
msgstr "איגוד הטכנולוגיסטים"

#. A large region of the world (Hereva) also known as land Ah, Shichimi's land.
msgid "the lands of the setting moons"
msgstr "ארץ הירחים השוקעים"

#. (Episode 16) The name of the planet and general setting Pepper&Carrot.
msgid "Hereva"
msgstr "חרבה"

#. (Episode 27) Coriander's city, a large industrial city standing alone in the
#. middle of desert on rocky bluff.
msgid "Qualicity"
msgstr "תל קואל"

#. (Episode 31) A hill or small mountain sacred to the school of Chaosah.
msgid "Tenebrume"
msgstr "טנברומה"

#. ~~~~~~ MAGIC: (Episode 8) The magic of chaos; that which Pepper now
#. practices.
msgid "Chaosah"
msgstr "קאוסה"

#. (Episode 18) The magic of plants and living creatures; that which Camomile
#. practices Pepper used to.
msgid "Hippiah"
msgstr "היפיה"

#. (Episode 21) The magic of giving life to dead things including machines;
#. that which Coriander practices.
msgid "Zombiah"
msgstr "זומביה"

#. The magic of fire, melting metals and cooking; that which Saffron practices.
msgid "Magmah"
msgstr "מגמה"

#. The magic of ghost and spirits; that which Shichimi practices.
msgid "Ah"
msgstr "אה"

#. The magic of water, rain, and oceans; that which Spirulina practices.
msgid "Aquah"
msgstr "אקווה"

#. (Episode 24) The substance or unit of magic, the name which is derived from
#. "reality".
msgid "Rea"
msgstr "ריה"

#. ~~~~~~ TIME SYSTEM: Weekday 1 − A day of recreation or rest, similar to
#. Sunday; associated with Chaosah, derived from the French word 'hazard'.
msgid "Azarday"
msgstr "יום אזר"

#. Weekday 2 - associated with Magmah, from babka, a sweet brioche cake.
msgid "Babkaday"
msgstr "יום בבקה"

#. Weekday 3 - associated with Aquah, from Ceto, a Greek mythological goddess
#. and sea monster.
msgid "Cetoday"
msgstr "יום סטו"

#. Weekday 4 - associated with Zombiah, from Donn, the lord of dead in Irish
#. myth.
msgid "Donday"
msgstr "יום דון"

#. Weekday 5 - associated with Hippiah, egg = life.
msgid "Eggday"
msgstr "יום ביצה"

#. Weekday 6 - associated with Ah, from the Japanese mythological god
#. Fukurokuju, fuku meaning "happiness".
msgid "Fookuday"
msgstr "יום הוק"

#. Weekday 7 - associated with all of the schools Hereva.
msgid "Zero's Day"
msgstr ""

#. Time - PM (afternoon)
msgid "Pinkmoon"
msgstr "ירח-ורוד"

#. Time - AM (morning)
msgid "Airmoon"
msgstr "אווירח"

#. ~~~~~~ MISC: (Episode 3) A vegetable at the market, a sort of squash shaped
#. like yellow star. Used as an ingredient in potions.
msgid "pumpkinstar"
msgstr "כוכב-דלעת"

#. The monetary unit of Komona city (and pun off 'kilo-octets', another word
#. for kilobytes).
msgid "Ko"
msgstr "קו"

#. A potion ingredient, obtained from clouds.
msgid "pearls of mist"
msgstr "פניני ערפל"

#. A large, docile creature: half-dragon, half-cow.
msgid "DragonCow"
msgstr "דרקון-פרה"

#. (Episode 14) A potion ingredient, a plant.
msgid "Dragon's Tooth"
msgstr "שן דרקון"

#. Name for a kind of dragon, blue and flying at high altitude.
msgid "Air Dragon"
msgstr "דרקון אוויר"

#. Name for a kind of dragon, covered with mud from the swamps.
msgid "Swamp Dragon"
msgstr "דרקון ביצות"

#. Name for a kind of dragon with body made electricity.
msgid "Lighting Dragon"
msgstr "דרקון ברקים"

#. (Episode 21) Name of the newspaper published in Komona city and distributed
#. across Hereva.
msgid "The Komonan"
msgstr "הקומונאן"

#. (Episode 24) Name for the moonlit meeting of Chaosah big decisions and
#. exams.
msgid "council of The Three Moons"
msgstr "מועצת שלושת הירחים"

#. (Episode 26) Name for a small sacred tree in the bottom of an abandoned
#. castle, generating continuous stream water.
msgid "Water-Tree"
msgstr "עץ-המים"
