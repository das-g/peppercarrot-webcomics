# Pepper&Carrot webcomics

This repository contains all the files necessary to translate the [Pepper&Carrot](https://www.peppercarrot.com) webcomics.
You'll mainly need the SVG here, the [fonts](https://framagit.org/peppercarrot/fonts) installed and the latest version of Inkscape to edit the SVG.

## Documentation

A full documentation about the workflow (written for beginner and advanced users) can be read into [the full documentation](https://www.peppercarrot.com/en/documentation).
You'll find also tips and explanations about the files.

## License

Content in this repository is licensed under the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) License. By submitting content to this repository, one agrees to the contributor's terms at the bottom of [CONTRIBUTING.md](CONTRIBUTING.md).

## References

To keep consistency for the names of characters and place accross all the webcomic's episodes, you can edit the LibreOffice spreadsheet file [translation-names-references.fods](translation-names-references.fods) at the root of the repository.

## Credits

Each translation contains a file named [info.json (translation info)](https://www.peppercarrot.com/en/static14/documentation&page=081_info.json).
They are hundreds on the repository and help at generating global credits as can be seen on the [translation status table](https://www.peppercarrot.com/en/static6/sources&page=translation)
or on [the credit generator](https://www.peppercarrot.com/en/static6/sources&page=credits).

## Artwork sources

This repository doesn't contain any artwork source file. The PNG you'll find here are available in low resolution to ease editing the translations.
Do not edit or open merge requests for the PNG artwork here.
The Krita sources files are available on the [0_sources directory](https://www.peppercarrot.com/0_sources/) of the server because they weight Gigabytes of data.

## Episode metadata

Each episode contains a metadata file called [info.json (Episode info)](https://www.peppercarrot.com/en/documentation/081_info.json.html).
This one doesn't need to be edited by translators.
