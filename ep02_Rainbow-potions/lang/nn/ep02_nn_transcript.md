# Transcript of Pepper&Carrot Episode 02 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 2: Regnbogetrylledrikkar

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|5|True|Glugg
Lyd|6|True|Glugg
Lyd|7|False|Glugg
Skrift|1|True|FARE
Skrift|3|False|PRIVAT OMRÅDE
Skrift|2|True|HEKS
Skrift|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|7|False|dunk|nowhitespace
Skrift|1|False|ELDD
Skrift|2|False|DJUPHAV
Skrift|3|False|FIOLETT
Skrift|4|False|ULTRAMARIN
Skrift|5|False|ROS
Skrift|6|False|DRAUD
Skrift|8|False|NATUR
Skrift|9|False|SOLGUL
Skrift|10|False|APPELSIN
Skrift|11|False|ELDDANS
Skrift|12|False|DJUPHAV
Skrift|13|False|FIOLETT
Skrift|14|False|ULTRAMARIN
Skrift|15|False|METAROSA
Skrift|16|False|MAGENTA X
Skrift|17|False|BLODRAUD

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|True|Glugg
Lyd|2|True|Glugg
Lyd|3|False|Glugg

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|True|Gulp
Lyd|2|False|Gulp
Lyd|3|True|Gulp
Lyd|4|False|Gulp
Lyd|5|True|Gulp
Lyd|6|False|Gulp
Lyd|21|False|m|nowhitespace
Lyd|20|True|m|nowhitespace
Lyd|19|True|M|nowhitespace
Lyd|7|True|S|nowhitespace
Lyd|8|True|P|nowhitespace
Lyd|9|True|l|nowhitespace
Lyd|10|False|urk!|nowhitespace
Lyd|11|True|S|nowhitespace
Lyd|12|True|S|nowhitespace
Lyd|13|True|S|nowhitespace
Lyd|14|True|P|nowhitespace
Lyd|15|True|l|nowhitespace
Lyd|16|True|a|nowhitespace
Lyd|17|True|t|nowhitespace
Lyd|18|False|t|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|True|B
Lyd|2|True|lubb|nowhitespace
Lyd|3|True|B
Lyd|4|False|lubb|nowhitespace
Lyd|7|True|upp|nowhitespace
Lyd|6|True|pl|nowhitespace
Lyd|5|True|s
Lyd|10|True|upp|nowhitespace
Lyd|9|True|pl|nowhitespace
Lyd|8|True|s
Lyd|13|False|upp|nowhitespace
Lyd|12|True|pl|nowhitespace
Lyd|11|True|s

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|True|Denne nettserien har opne kjeldefiler, og episoden vart finansiert av dei 21 støttespelarane mine på
Bidragsytarar|2|False|www.patreon.com/davidrevoy
Bidragsytarar|3|False|Tusen takk til
Bidragsytarar|4|False|Laga med Krita på GNU/Linux
