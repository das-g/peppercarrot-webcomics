# Transcript of Pepper&Carrot Episode 02 [pt]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episódio 2: Poções Arco-íris

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Som|5|True|Glup
Som|6|True|Glup
Som|7|False|Glup
Escrita|1|True|CUIDADO
Escrita|3|False|DE BRUXA
Escrita|2|True|PROPRIEDADE
Escrita|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Som|7|False|tok|nowhitespace
Escrita|1|False|FIRE D
Escrita|2|False|DEEP OCEAN
Escrita|3|False|VIOLE(N)T
Escrita|4|False|ULTRA BLUE
Escrita|5|False|PIN
Escrita|6|False|MSON
Escrita|8|False|NATURE
Escrita|9|False|YELLOW
Escrita|10|False|ORANGE TOP
Escrita|11|False|FIRE DANCE
Escrita|12|False|DEEP OCEAN
Escrita|13|False|VIOLE(N)T
Escrita|14|False|ULTRA BLUE
Escrita|15|False|META PINK
Escrita|16|False|MAGENTA X
Escrita|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Som|1|True|Glup
Som|2|True|Glup
Som|3|False|Glup

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Som|1|True|Gulp
Som|2|False|Gulp
Som|3|True|Gulp
Som|4|False|Gulp
Som|5|True|Gulp
Som|6|False|Gulp
Som|20|False|m|nowhitespace
Som|19|True|m|nowhitespace
Som|18|True|M|nowhitespace
Som|7|True|S|nowhitespace
Som|8|True|P|nowhitespace
Som|9|True|l|nowhitespace
Som|10|False|urp !|nowhitespace
Som|11|True|S|nowhitespace
Som|12|True|S|nowhitespace
Som|13|True|S|nowhitespace
Som|14|True|P|nowhitespace
Som|15|True|l|nowhitespace
Som|16|True|a|nowhitespace
Som|17|False|t|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Som|1|True|B
Som|2|True|lup|nowhitespace
Som|3|True|B
Som|4|False|lup|nowhitespace
Som|7|True|up|nowhitespace
Som|6|True|pl|nowhitespace
Som|5|True|s
Som|10|True|up|nowhitespace
Som|9|True|pl|nowhitespace
Som|8|True|s
Som|13|False|up|nowhitespace
Som|12|True|pl|nowhitespace
Som|11|True|s

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|True|Essa webcomic é open-source, e esse episódio foi patrocinado pelos meus 21 Patronos em
Créditos|2|False|www.patreon.com/davidrevoy
Créditos|3|False|Muito obrigado a
Créditos|4|False|Produzido com Krita e GNU/Linux
