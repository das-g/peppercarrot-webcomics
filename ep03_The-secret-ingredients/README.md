﻿# Episode 3: The Secret Ingredients

![cover of episode 3](lang/gfx_Pepper-and-Carrot_by-David-Revoy_E03.png)

2016-01-08: To compile this episode before the 2016 refactoring, you'll need older Krita sources files in 2016-01-08_ep03_Potion-of-Flight.zip (available in /src folder of the episode [https://www.peppercarrot.com/0_sources/](https://www.peppercarrot.com/0_sources/)). It's a patch for previous page 2 geometry.
