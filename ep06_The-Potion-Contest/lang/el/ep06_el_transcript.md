# Transcript of Pepper&Carrot Episode 06 [el]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Επεισόδιο 6: Ο Διαγωνισμός Φίλτρου

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Πιπεριά|1|True|Ωχ φτου... Κοιμήθηκα με ανοιχτό παράθυρο, πάλι...
Πιπεριά|2|True|είναι τόσο...
Πιπεριά|3|False|...και γιατί μπορώ να δω την πόλη Κόμονα από το παράθυρο;
Πιπεριά|4|False|ΚΟΜΟΝΑ!
Πιπεριά|5|True|Ο διαγωνισμός φίλτρου!
Πιπεριά|6|True|Μάλλον... Μάλλον παρακοιμήθηκα καταλάθως...
Πιπεριά|9|True|...αλλά;
Πιπεριά|10|False|Πού βρίσκομαι;!!
Bird|12|False|ΚουάΚ;!|nowhitespace
Πιπεριά|7|False|*|nowhitespace
Note|8|False|*Βλέπε επεισόδιο 4

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Πιπεριά|1|False|!!!
Πιπεριά|2|False|Καροτούλη!!! Πολύ σωστή η σκέψη σου να μας "πετάξεις" ως την Κόμονα!
Πιπεριά|3|False|Υ-πέ-ρο-χα!
Πιπεριά|4|True|Σκέφτηκες ακόμη και να φέρεις ένα φίλτρο, τα ρούχα μου και το κάπέλο μου!
Πιπεριά|5|False|...για να δούμε τι είδους φίλτρο έφερες...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Πιπεριά|1|False|ΤΙΙ;;;
Mayor of Komona|3|False|Ως Δήμαρχος της Κόμονα, ανακοινώνω την έναρξη του Διαγωνισμού Φίλτρου...
Mayor of Komona|4|False|Η πόλη μας υποδέχεται θερμά όχι μια, ούτε δυό, αλλά ΤΕΣΣΕΡΕΙΣ μάγισσες για τον διαγωνισμό...
Mayor of Komona|5|True|Παρακαλώ δώστε ένα
Mayor of Komona|6|True|θερμό
Writing|2|False|Διαγωνισμός Φίλτρου της Κόμονα
Mayor of Komona|7|False|χειροκρότημα για τις:

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Κλαπ
Mayor of Komona|1|True|Από την ξακουστή Ένωση Τεχνολόγων, είναι τιμή μας να υποδεχθούμε τη μαγευτική και πανέξυπνη
Mayor of Komona|3|True|χωρίς να ξεχνάμε την δική μας μάγισσα, γέννημα-θρέμα της Κόμονα, την
Mayor of Komona|5|True|...Η τρίτη μας διαγωνιζώμενος έρχεται πέρα μακριά, από τις πεδιάδες των κοιμώμενων Φεγγαριών, η
Mayor of Komona|7|True|...και τέλος, η τελευταία μας διαγωνιζόμενος, από το δάσος της Ουράς του Τρελλού Σκίουρου, η
Mayor of Komona|2|False|Κοριάντερ!
Mayor of Komona|4|False|Σάφρον!
Mayor of Komona|6|False|Σίσσιμι!
Mayor of Komona|8|False|Πιπεριά!
Mayor of Komona|9|True|Ας αρχίσει ο διαγωνισμός!
Mayor of Komona|10|False|Οι ψήφοι θα μετρηθούν με το χειροκρότημα.
Mayor of Komona|11|False|Πρώτα, η επίδειξη της Κοριάντερ...
Κοριάντερ|13|False|...βάλτε τέλος στον θάνατο, χάρη στο...
Κοριάντερ|14|True|φίλτρο
Κοριάντερ|15|False|ΖΟΜΠΟΠΟΙΗΣΗΣ!!!
Audience|16|True|Κλαπ
Audience|17|True|Κλαπ
Audience|18|True|Κλαπ
Audience|19|True|Κλαπ
Audience|20|True|Κλαπ
Audience|21|True|Κλαπ
Audience|22|True|Κλαπ
Audience|23|True|Κλαπ
Audience|24|True|Κλαπ
Audience|25|True|Κλαπ
Audience|26|True|Κλαπ
Audience|27|True|Κλαπ
Audience|28|True|Κλαπ
Κοριάντερ|12|False|Κυρίες και κύριοι...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|ΦΟΒΕΡΟ!!!
Audience|3|True|Κλαπ
Audience|4|True|Κλαπ
Audience|5|True|Κλαπ
Audience|6|True|Κλαπ
Audience|7|True|Κλαπ
Audience|8|True|Κλαπ
Audience|9|True|Κλαπ
Audience|10|True|Κλαπ
Audience|11|True|Κλαπ
Audience|12|True|Κλαπ
Audience|13|True|Κλαπ
Audience|14|True|Κλαπ
Audience|15|True|Κλαπ
Audience|16|False|Κλαπ
Σάφρον|18|True|Ιδού το
Σάφρον|17|True|...αλλά σας παρακαλώ, πολίτες της Κόμονα, φυλάξτε το χειροκρότημα σας...
Σάφρον|22|False|...και θα τους κάνει να ζηλέψουν!
Σάφρον|19|True|ΔΙΚΟ ΜΟΥ
Σάφρον|25|False|ΨΙΛΟΜΥΤΙΑΣ!!!
Σάφρον|24|True|...φίλτρο της
Σάφρον|23|False|...όλα αυτά γίνονται πραγματικότητα με μία μικρή σταγόνα από το...
Audience|26|True|Κλαπ
Audience|27|True|Κλαπ
Audience|28|True|Κλαπ
Audience|29|True|Κλαπ
Audience|30|True|Κλαπ
Audience|31|True|Κλαπ
Audience|32|True|Κλαπ
Audience|33|True|Κλαπ
Audience|34|True|Κλαπ
Audience|35|True|Κλαπ
Audience|36|True|Κλαπ
Audience|37|True|Κλαπ
Audience|38|True|Κλαπ
Audience|39|True|Κλαπ
Audience|40|False|Κλαπ
Mayor of Komona|42|False|Αυτό το φίλτρο θα μας κάνει όλους πλούσιους!
Mayor of Komona|41|True|Φαντασιτκό! Υπέροχο!
Audience|44|True|Κλαπ
Audience|45|True|Κλαπ
Audience|46|True|Κλαπ
Audience|47|True|Κλαπ
Audience|48|True|Κλαπ
Audience|49|True|Κλαπ
Audience|50|True|Κλαπ
Audience|51|True|Κλαπ
Audience|52|True|Κλαπ
Audience|53|True|Κλαπ
Audience|54|True|Κλαπ
Audience|55|True|Κλαπ
Audience|56|True|Κλαπ
Audience|57|True|Κλαπ
Audience|58|True|Κλαπ
Audience|59|True|Κλαπ
Audience|60|False|Κλαπ
Mayor of Komona|2|False|Η Κοριάντερ κοροϊδεύει τον ίδιο τον θάνατο μέ αυτό το θαυ-μα-τουρ-γό φίλτρο!
Σάφρον|21|True|Το φίλτρο που ΟΛΟΙ σας περιμένατε: αυτό που θα τυφλώσει τους γείτονές σας...
Mayor of Komona|43|False|Το χειροκρότημα σας δε λέει ψέματα. Η Κοριάντερ έχει ήδη αποκλισθεί.
Σάφρον|20|False|φιλτρο

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Η τελευταία παρουσίαση είναι δύσκολο να ανατραπεί από την Σίσσιμι!
Σίσσιμι|4|True|ΟΧΙ!
Σίσσιμι|5|True|Δεν μπορώ, Είναι πολύ επικίνδυνο...
Σίσσιμι|6|False|ΣΥΓΝΩΜΗ!
Mayor of Komona|3|False|...εμπρός Σίσσιμι όλοι περιμένουν με ανυπομονησία...
Mayor of Komona|7|False|Απ' ό,τι φαίνεται, κυρίες και κύριοι, η Σίσσιμι αποσύρεται...
Σάφρον|8|False|Δώσ'το μου αυτό!
Σάφρον|9|False|...και σταμάτα να παριστάνεις την ντροπαλη, μας χαλάς το σόου...
Σάφρον|10|False|Όλοι ξέρουν πως εγώ έχω νικήσει, ό,τι και να κάνει το χαζό σου φίλτρο...
Σίσσιμι|11|False|!!!
Sound|12|False|ΓΖΟ ΟΥ Ο Υ Υ|nowhitespace
Σίσσιμι|15|False|ΤΕΡΑΤΟΠΟΙΗΣΗΣ!!!
Σίσσιμι|2|False|Δεν... Δεν ήξερα πως έπρεπε να δώσουμε παρουσίαση...
Σίσσιμι|13|True|ΠΡΟΣΕΧΤΕ!!!
Σίσσιμι|14|True|Είναι φίλτρο

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|κοΚΟΟΥ-ΚΟ-κο-Κο οΥ|nowhitespace
Sound|2|False|ΜΠΑΜ!
Πιπεριά|3|True|...ααα! Πολύ ωραίο...
Πιπεριά|5|False|το φίλτρο μου τουλάχιστον θα σας κάνει να γελάσετε, διότι...
Πιπεριά|4|False|σείρα μου τώρα, έτσι;
Mayor of Komona|6|True|Τρέχα, μουρλάθηκες;
Mayor of Komona|7|False|Ο διαγωνισμός τελείωσε! ...ο σώζων εαυτώ σωθήτω!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Πιπεριά|2|False|όπως πάντα, όλοι φεύγουν όταν έρχεται η σειρά μας...
Πιπεριά|1|True|πάλι τα ίδια...
Πιπεριά|4|True|Τουλάχιστον έχω μια ιδέα με το τι μπορούμε να κάνουμε με το "φίλτρο" μας, Καροτούλη...
Πιπεριά|5|False|...θα σώσουμε τη μέρα και μετά πάμε σπίτι!
Πιπεριά|7|False|Υπερμεγέθες-Ψιλομύτικο-Ζόμπο-Καναρίνι!
Πιπεριά|9|False|Θες να δοκιμάσεις άλλο ένα φίλτρο;;;
Πιπεριά|10|False|...όχι ιδιαίτερα, ε;
Πιπεριά|6|False|ΈΙ ΕΣΥ!
Sound|8|False|ΚΡΑΚ!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Πιπεριά|1|True|Έεετσι, διάβασε την ετικέτα προσεκτικά...
Πιπεριά|2|False|...Δεν θα διστάσω ούτε στιγμή να το ρίξω όλο πάνω σου, αν δεν φύγεις από την Κόμονα αυτή τη στιγμή!
Mayor of Komona|3|True|...Και επειδή έσωσε την πόλη μας όταν βρισκόταν σε κίνδυνο...
Mayor of Komona|4|False|Το πρώτο βραβείο πάει στην Πιπεριά για το φίλτρο...;
Πιπεριά|7|False|...λοιπόν... για την ακρίβεια, δεν είναι "ακριβώς" φίλτρο... είναι δείγμα ούρων της γάτας μου από την τελευταία επίσκεψή μας στον κτηνίατρο!
Πιπεριά|6|True|...Χαχα! Εμμ...
Πιπεριά|8|False|δεν θέλετε παρουσίαση, υποθέτω...;
Narrator|9|False|Επεισόδιο 6 : Ο Διαγωνισμός Φίλτρου
Narrator|10|False|ΤΕΛΟΣ
Writing|5|False|50,000 Ko
Credits|11|False|Μάρτιος 2015 - Σκίτσο και Σενάριο από David Revoy - Μετάφραση από Γεώργιο Καρέττα

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Η Πιπεριά και Καροτούλης είναι ένα εντελώς δωρεάν και ανοικτού κώδικα κόμικ, που υποστηρίζεται με χορηγίες. Αυτό το επεισόδιο χρηματοδοτήθηκε από 245 χορηγούς:
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Και εσύ μπορείς να γίνεις χορηγός για το επόμενο επεισόδιο:
Credits|7|False|Εργαλεία: Το κόμικ δημιουργήθηκε με 100% δωρεάν λογισμικό, όπως το Krita σε Linux Mint
Credits|6|False|Ακοικτού Κώδικα: Όλα τα εργαλεία βρίσκονται στο site
Credits|5|False|Άδεια : Creative Commons Attribution μπορείτε να κάνετε αλλαγές, να το επανεκδώσετε, να πουλήσετε προϊόντα σχετικά με το κόμικ κλπ...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
