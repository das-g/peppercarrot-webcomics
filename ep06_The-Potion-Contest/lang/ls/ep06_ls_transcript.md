# Transcript of Pepper&Carrot Episode 06 [ls]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 6: Concurso de pociones

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|¡Oh rayos!, he vuelto a dejar la ventana abierta mientras dormía...
Pepper|2|True|...Hay mucho viento...
Pepper|3|False|Y... ¿por qué puedo ver Komona desde la ventana?
Pepper|4|False|¡KOMONA!
Pepper|5|False|¡El concurso de pociones!
Pepper|6|False|Yo me quedé... ¡me quedé dormida accidentalmente!
Pepper|7|True|...¿Pero?
Pepper|8|False|¿!Dónde estoy!?
Aves|10|False|?|nowhitespace
Aves|9|True|¿cua|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|¡Carrot! ¡Eres tan lindo, quién pensaría que me traerías volando al concurso!
Pepper|3|False|¡Fan-tás-ti-co!
Pepper|4|True|Incluso has traído una poción, mi ropa y mi sombrero...
Pepper|5|False|...Veamos cuál poción has traído...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|¡¡¿QUÉ?!!
Alcalde de Komona|3|False|Como alcalde de Komona, ¡declaro el inicio del concurso de pociones!
Alcalde de Komona|4|False|Nuestro pueblo está encantado de darle la bienvenida a las cuatro brujas que participarán en la primera edición.
Alcalde de Komona|5|True|Por favor denles un
Alcalde de Komona|6|True|fuerte
Texto|2|False|Concurso de pociones de Komona
Alcalde de Komona|7|False|aplauso.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audiencia|29|False|Clap
Alcalde de Komona|1|True|¡Desde el gran Sindicato de Tecnólogos, es un honor darle la bienvenida a la deslumbrante e ingeniosa,
Alcalde de Komona|3|True|...No sin olvidar a nuestra participante, ¡la bruja más querida de Komona,
Alcalde de Komona|5|True|...¡La tercera participante viene desde las Tierras de las Lunas Ponientes,
Alcalde de Komona|7|True|...Y finalmente, nuestra última participante, ¡desde el Bosque Cola de Ardilla,
Alcalde de Komona|2|False|Coriander!
Alcalde de Komona|4|False|Saffron!
Alcalde de Komona|6|False|Shichimi!
Alcalde de Komona|8|False|Pepper!
Alcalde de Komona|9|True|¡Que comience el concurso!
Alcalde de Komona|10|False|Los votos se decidirán con el aplausómetro.
Alcalde de Komona|11|False|Primero, la demostración de Coriander
Coriander|13|False|...No le teman más a la muerte, gracias a mi...
Coriander|14|True|...¡Poción de
Coriander|15|False|ZOMBIFICACIÓN!
Audiencia|16|True|Clap
Audiencia|17|True|Clap
Audiencia|18|True|Clap
Audiencia|19|True|Clap
Audiencia|20|True|Clap
Audiencia|21|True|Clap
Audiencia|22|True|Clap
Audiencia|23|True|Clap
Audiencia|24|True|Clap
Audiencia|25|True|Clap
Audiencia|26|True|Clap
Audiencia|27|True|Clap
Audiencia|28|True|Clap
Coriander|12|False|Damas y caballeros...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Alcalde de Komona|1|False|¡FANTÁSTICO!
Audiencia|3|True|Clap
Audiencia|4|True|Clap
Audiencia|5|True|Clap
Audiencia|6|True|Clap
Audiencia|7|True|Clap
Audiencia|8|True|Clap
Audiencia|9|True|Clap
Audiencia|10|True|Clap
Audiencia|11|True|Clap
Audiencia|12|True|Clap
Audiencia|13|True|Clap
Audiencia|14|True|Clap
Audiencia|15|True|Clap
Audiencia|16|False|Clap
Saffron|18|True|porqué aquí está
Saffron|17|True|...Por favor, ¡gente de Komona, guarden sus aplausos!
Saffron|22|False|...¡los harán sentirse celosos!
Saffron|19|True|MI
Saffron|25|False|ELEGANCIA!
Saffron|24|True|...¡Poción de
Saffron|23|False|...Todo esto es posible al aplicar una sola gota de mi...
Audiencia|26|True|Clap
Audiencia|27|True|Clap
Audiencia|28|True|Clap
Audiencia|29|True|Clap
Audiencia|30|True|Clap
Audiencia|31|True|Clap
Audiencia|32|True|Clap
Audiencia|33|True|Clap
Audiencia|34|True|Clap
Audiencia|35|True|Clap
Audiencia|36|True|Clap
Audiencia|37|True|Clap
Audiencia|38|True|Clap
Audiencia|39|True|Clap
Audiencia|40|False|Clap
Alcalde de Komona|42|False|¡Esta poción podría hacer a toda Komona millonaria!
Alcalde de Komona|41|True|¡Fantástico! ¡increíble!
Audiencia|44|True|Clap
Audiencia|45|True|Clap
Audiencia|46|True|Clap
Audiencia|47|True|Clap
Audiencia|48|True|Clap
Audiencia|49|True|Clap
Audiencia|50|True|Clap
Audiencia|51|True|Clap
Audiencia|52|True|Clap
Audiencia|53|True|Clap
Audiencia|54|True|Clap
Audiencia|55|True|Clap
Audiencia|56|True|Clap
Audiencia|57|True|Clap
Audiencia|58|True|Clap
Audiencia|59|True|Clap
Audiencia|60|False|Clap
Alcalde de Komona|2|False|Coriander desafía a la misma muerte, con su ¡mi-la-gro-sa poción!
Saffron|21|True|La poción que todos estaban esperando, con la que sorprenderán a sus vecinos...
Alcalde de Komona|43|False|Sus aplausos no dejan ninguna duda. Coriander ha sido eliminada.
Saffron|20|False|poción.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Alcalde de Komona|1|False|¡La última demostración será difícil de superar para Shichimi!
Shichimi|4|True|¡NO!
Shichimi|5|True|No puedo, es demasiado peligroso.
Shichimi|6|False|¡LO SIENTO!
Alcalde de Komona|3|False|...Vamos Shichimi, todos están esperando por ti.
Alcalde de Komona|7|False|Parece ser, damas y caballeros que Sichimi se retira de la competición...
Saffron|8|False|¡Dame eso!
Saffron|9|False|...Y deja de fingir que eres tímida, estás arruinando el espectáculo.
Saffron|10|False|Todos saben que ya gané el concurso, así que no importa lo que haga tu poción...
Shichimi|11|False|!!!
Sonido|12|False|B Z Z Z I IO O|nowhitespace
Shichimi|15|False|MONSTRUOS GIGANTES!
Shichimi|2|False|Yo... No sabía que teníamos que hacer una demostracíon.
Shichimi|13|True|¡¡CUIDADO!!!
Shichimi|14|True|¡Es una poción para crear

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aves|1|False|CAW-CAW-C aa aa aa aa w w ww|nowhitespace
Sonido|2|False|BAM!
Pepper|3|True|...¡Uy, genial!
Pepper|5|False|...Mi poción los hará reir un poco porqué...
Pepper|4|False|entonces, ¿es mi turno ahora?
Alcalde de Komona|6|True|¡Corre, tonta!
Alcalde de Komona|7|False|¡La competición terminó!... ¡sálvate como puedas!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|...como siempre, todos se van justo cuando es nuestro turno.
Pepper|1|True|Ahí lo tienes...
Pepper|4|True|Al menos creo que tengo una idea de que podríamos hacer con tu "poción", Carrot.
Pepper|5|False|...¡Poner todo en orden y luego regresar a casa!
Pepper|7|True|Tú
Pepper|8|False|¡Canario-zombi-elegante-gigante!
Pepper|10|False|¿Quieres probar una poción más?...
Pepper|11|False|...¿No realmente, verdad?
Pepper|6|False|¡HEY!
Sonido|9|False|C R AC K !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Sí, lee cuidadosamente la etiqueta...
Pepper|2|False|...¡No lo pensaré dos veces antes de derramarlo sobre ti si no sales de Komona en este instante!
Alcalde de Komona|3|True|Por salvar a nuestra ciudad cuando estaba en peligro,
Alcalde de Komona|4|False|le otorgamos a Pepper el primer lugar ¡¡¿¿por su poción de...??!!
Pepper|7|False|...uh... en realidad, no es una poción; ¡es una muestra de orina de la última visita de mi gato al veterinario!
Pepper|6|True|...¡ja,ja! sip...
Pepper|8|False|...así que, ¿no hay demostración?...
Narrador|9|False|Episodio 6: Concurso de pociones
Narrador|10|False|FIN
Texto|5|False|50,000 Ko
Créditos|11|False|Marzo 2015 - Historia e ilustraiciones por David Revoy - Tradución por Lewatoto

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|Pepper&Carrot es totalmente gratuito, de código abierto y patrocinado gracias a nuestros amables lectores. Para este episodio, muchas gracias a los 245 mecenas:
Créditos|4|False|https://www.patreon.com/davidrevoy
Créditos|3|False|Tú también puedes convertirte en mecenas del siguiente episodio de Pepper&Carrot en:
Créditos|7|False|Herramientas: este episodio fue realizado con las herramientas libres y de código abierto Krita en Linux Mint.
Créditos|6|False|Código abierto: los archivos originales y las tipografías, se encuentran disponibles en el sitio oficial.
Créditos|5|False|Licencia : Creative Commons Atribución. Puedes modificarlo, resubirlo, venderlo, etc....
Créditos|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
