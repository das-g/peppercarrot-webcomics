# Transcript of Pepper&Carrot Episode 08 [ls]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 8: La fiesta de cumpleaños de Pepper

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Se acerca mi cumpleaños y no tengo amigos con quienes celebrarlo...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|¡Oh, entiendo!, ¿invitamos a las brujas que conocimos en el concurso de pociones?*
Pepper|3|False|¡Gran idea Carrot!
Texto|4|False|Invitación
Nota|2|False|* Ver el Episodio 6: Concurso de pociones

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Y con esto terminamos...
Pepper|2|False|...todo está listo, lo único que debemos hacer es esperar a que vengan.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|...que vendrían...
Pepper|1|True|Pero estaba segura... Estaba segura...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|¡¡¿Cómo pudieron hacerme esto?!! ¡¡¡A
Pepper|2|True|mí
Texto|4|False|Encantamientos para Demonios de CHAOSAH Vol .1
Pepper|5|False|...ya que no puedo contar con ellas...
Sonido|6|False|D Z Z ZO O O|nowhitespace
Pepper|3|False|!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monstruos|1|False|¡Los Monstruos de Chaosah a tú servicio!
Monstruos|2|True|¿A quién quieres que
Monstruos|3|False|ataquemos?...
Narrador|4|False|Episodio 8: La fiesta de cumpleaños de Pepper
Narrador|5|False|FIN
Créditos|6|False|Junio 2015 - www.peppercarrot.com - Historia e ilustraciones por David Revoy - Traducción Lewatoto

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|Pepper&Carrot es totalmente gratuito, de código abierto y patrocinado gracias a nuestros amables lectores. Para este episodio, muchas gracias a los 354 mecenas:
Créditos|4|False|https://www.patreon.com/davidrevoy
Créditos|3|True|Tú también puedes convertirte en mecenas del siguiente episodio de Pepper&Carrot en:
Créditos|7|False|Herramientas: este episodio fue realizado con las herramientas libres y de código abierto Krita, Inkscape en GNU/Linux.
Créditos|6|False|Código abierto: los archivos orginales se encuentran en el sitio web oficial.
Créditos|5|False|Licencia: Creative Commons Atribución. Puedes modificarlo, resubirlo, venderlo, etc....
Créditos|2|False|Глеб Бузало - 獨孤欣 & 獨弧悦 - Adam - Addison Lewis - A Distinguished Robot - Adrian Lord - Ahmad Ali - Aina Reich - al - Alandran Alan Hardman - Albert Westra - Alcide - Alex - Alexander Bülow Tomassen - AlexanderKennedy - Alexander Sopicki - Alexandra Jordan Alex Bradaric - Alexey Golubev - Alex Kotenko - Alex Lusco - Alex Silver - Alex V - Alfredo - Ali Poulton (Aunty Pol) - Allan Zieser - Alok Baikadi Andreas Rieger - Andreas Ulmer - Andrej Kwadrin - Andrew - Andrew Godfrey - Andrey Alekseenko - Andy Gelme - Anna Orlova - anonymous Antan Karmola - Anthony Edlin - Antoine - Antonio Mendoza - Antonio Parisi - Ardash Crowfoot - Arjun Chennu - Arne Brix - Arnulf - Arturo J. Pérez Axel Bordelon - Axel Philipsenburg - Ayaskull - barbix - BataMoth - Bela Bargel - Ben Evans - Bernd - Bernhard Saumweber - Betsy Luntao Birger Tuer Thorvaldsen - blacksheep33512 - Boonsak Watanavisit - Boris Fauret - Boudewijn Rempt - BoxBoy - Brent Houghton - Brett Brett Smith - Brian Behnke - Bryan Butler - Bryan Rosander - BS - Bui Dang Hai Trieu - BXS - carlos levischi - Cedric Wohlleber - Charles Charlotte Lacombe-bar - Chris - Chris Radcliff - Chris Sakkas - Christian Gruenwaldner - Christophe Carré - Christopher Bates Christopher Rodriguez - Clara Dexter - codl - Colby Driedger - Conway Scott Smith - Coppin Olivier - Cuthbert Williams - Cyol Cyrille Largillier - Cyril Paciullo - Damien - Daniel - Daniel Björkman - Daniel Lynn - Danny Grimm - Dan Stolyarov - David - David Kerdudo David Tang - Davi Na - Davis Aites - Dawn Blair - DecMoon - Dezponia Veil - DiCola Jamn - Dmitry - Donald Hayward - Douglas Oliveira Pessoa Doug Moen - Duke - Eitan Goldshtrom - Ejner Fergo - Enrico Billich - Enrique Lopez - epsilon - Eric Schulz - Erik Moeller Esteban Manchado Velázquez - Faolan Grady - Fen Yun Fat - Francois Schnell - francou - freecultureftw - Garret Patterson - Gary Thomas Ginny Hendricks - GreenAngel5 - Grigory Petrov - G. S. Davis - Guillaume - Gustav Strömbom - Happy Mimic - Helmar Suschka - Henning Döscher Henry Ståhle - HobbyThor - Igor - Ilyas - Irina Rempt - Ivan Korotkov - Jacob - James Frazier - Jamie Sutherland - Janusz - Jared Tritsch Jason - JDB - Jean-Baptiste Hebbrecht - Jean-Gabriel Loquet - Jeffrey Schneider - Jessey Wright - Jessica Gadling - Jhonny Rosa - Jim Jim Street - Jiska - Joao Luiz - Joerg Raidt - Joern Konopka - joe rutledge - John - John - John Gholson - John Urquhart Ferguson Jónatan Nilsson - Jonathan Leroy - Jonathan Ringstad - Jon Brake - Jorge Bernal - Joseph Bowman - Josh Cavalier - Juju Mendivil - Julia Velkova Julio Avila - Justus Kat - Kailyce - Kai-Ting (Danil) Ko - Kate - Kathryn Wuerstl - Ken Mingyuan Xia - Kevin Estalella - Kevin Trévien - Kingsquee Kroet - Kurain - La Plume - Lars Ivar Igesund - Lenod - Levi Kornelsen - Liang - Liselle - Lise-Lotte Pesonen - Lloyd Ash Pyne - Lorentz Grip Lorenzo Leonini - Louis Yung - Luc Stepniewski - Luke Hochrein - Magnus Kronnäs - Mahwiii - Manuel - Manu Järvinen - Marc et Rick Marco Sousa - marcus - Martin Owens - Mary Brownlee - Masked Admirer - Matthew Reynolds - Matt Lichtenwalner - mefflin ross bullis-bates Michael - Michael F. SChönitzer - Michael Gill - Michael Pureka - Michelle Pereira Garcia - Mike Mosher - Miriam Varón - Miroslav - mjkj Moritz Fuchs - Muriah Summer - Nazhif - Nicholas DeLateur - Nicholas Terranova - Nicki Aya - Nicola Angel - Nicolae Berbece - Nicole Heersema Nielas Sinclair - NinjaKnight Comics - Noble Hays - Noelia Calles Marcos - Nora Czaykowski - No Reward - Nyx - Oleg Schelykalnov Olga Bikmullina - Olivier Amrein - Olivier Brun - Olivier De Rop - Olivier Gavrois - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. Pat David - Patrick Dezothez - Patrick Gamblin - Paul - Paul - Pavel Semenov - Pet0r - Peter - Peter Moonen - Petr Vlašic Philippe Jean Edward Bateman - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Praveen Bhamidipati - Pyves & Ran - Raghavendra Kamath Rajul Gupta - Ramel Hill - Raymond Fullon - Ray Powell - Rebecca Morris - Reorx Meng - Ret Samys - Reuben Tracey - Ricardo Muggli - rictic RJ van der Weide - Roberto Zaghis - Robin Moussu - Roman Burdun - Rose “flash” Flashis - Rumiko Hoshino - Rustin Simons - Sally Bridgewater Sami T - Samuel Mitson - Scott Petrovic - Sean Adams - Sebastien - Sevag Bakalian - ShadowMist - shafak - Shawn Meyer - Simon Forster Simon Isenberg - Sonja Reimann-Klieber - Sonny W. - Soriac - Stanislav German-Evtushenko - Stanislav Vodetskyi - Stephan Theelke Stephen Bates - Stephen Smoogen - Steven Bennett - Stuart Dickson - surt - Taedirk - TamaskanLEM - tar8156 - Tas Kartas - Terry Hancock TheFaico - thibhul - Thomas Citharel - Thomas Courbon - Thomas Schwery - Thor Galle - Thornae - Tim Burbank - Tim J. - Tomas Hajek Tom Savage - Travis Humble - tree - Tristy - Tyson Tan - uglyheroes - Urm - usfreitas - Vera Vukovic - Victoria - Victoria White Vladislav Kurdyukov - Vlad Tomash - WakoTabacco - Wander - Westen Curry - Wilhelmine Faust - Xavier Claude - Yalyn Vinkindo - Yaroslav Yasmin - Zeni Pong - Źmicier Kušnaroŭ - zubr kabbi.
