# Transcript of Pepper&Carrot Episode 13 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 13: Pyjamasparty

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|4|False|Det er bare SÅ godt !
Pepper|5|False|Tak fordi du inviterede os, Koriander!
Pepper|3|False|… nogensinde!
Pepper|1|True|Det er den bedste…
Pepper|2|True|… ferie…

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|1|True|Selv tak!
Koriander|2|False|Jeg håber ikke, at I er alt for overrumplede over mit personale
Pepper|11|False|Nej, bare rolig; de er super gæstfrie. Man føler sig straks hjemme.
Shichimi|13|False|… så raffineret og hyggelig på samme tid!
Pepper|14|False|Helt enig!
Shichimi|12|True|Jeg er virkelig imponeret over indretningen…
Monster|9|False|HoooOoh ! ! !|nowhitespace
Monster|10|False|HoOoh ! ! !|nowhitespace
Monster|8|False|HoooOoh ! ! !|nowhitespace
Lyd|7|False|Shwwwwwwiiing!|nowhitespace
Lyd|6|False|Klaf!|nowhitespace
Lyd|5|False|Klukf!|nowhitespace
Lyd|3|False|Dzzziii ! ! !|nowhitespace
Lyd|4|False|Shkak ! ! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|2|True|Jeg synes nogle gange, at det er så...
Shichimi|5|True|Jeg ville meget gerne bo i sådan en komfort,
Lyd|7|False|Shiingz ! ! !|nowhitespace
Lyd|4|False|blum!|nowhitespace
Shichimi|8|False|… ”En ægte Ah-heks kan ikke leve i sådan en overdådighed”
Pepper|9|True|Ha ha!...
Pepper|10|True|Traditionernes bånd...
Pepper|11|False|Det lyder som en sætning fra en af mine gudmødre!
Shichimi|12|False|Er det rigtig?
Koriander|1|True|Tak fordi du minder mig om det
Koriander|3|False|... "banalt"...
Shichimi|6|False|men...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|2|False|Kom så piger! Lad os fokusere; vi er næsten i mål
Pepper & Shichimi|1|False|Hi hi hi!
Pepper|3|True|Slap af…!
Pepper|5|False|Vi er ikke engang begyndt på...
Lyd|6|False|VRoooOwwoww ! ! !|nowhitespace
Lyd|9|False|Shhshh|nowhitespace
Shichimi|8|False|?!!
Koriander|7|False|PEPPER! !!|nowhitespace
Pepper|4|True|Denne mission er alligevel supernem…

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|6|False|K R A K! !|nowhitespace
Shichimi|8|False|Åh NEJ!
Koriander|9|False|Ikke igen!
Pepper|11|False|Giv slip! Med det samme!
Koriander|1|False|...Åh nej! Det er for sent! Hun... Hun er...
Shichimi|2|False|N e e ej!!!|nowhitespace
Monster|3|False|MUAH HAHA HAHA !!!|nowhitespace
Shichimi|4|False|G RR ! ! !|nowhitespace
Koriander|5|False|Det skal du BETALE for!...
Lyd|7|False|KLING ! !|nowhitespace
Lyd|13|False|P LO NK ! !|nowhitespace
Lyd|12|False|P AF! !|nowhitespace
Pepper|10|True|CARROT!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|10|False|- Slut -
Lyd|1|False|11/2015 – Tegning og manuskript: David Revoy – Oversættelse: Emmiline, Rikke & Alexandre Alapetite|nowhitespace
Pepper|4|True|Bum
Pepper|3|True|Jeg ved, at du gør det for at beskytte mig...
Pepper|5|False|Kom nu... Knur ikke!
Carrot|8|False|... men det er ikke nødvendigt når vi leger!
Skrift|6|False|Grrrr
Skrift|2|False|Citadeller & Fønikser
Fortæller|9|False|Prinsesse Koriander

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot er helt gratis, open-source, og sponsoreret af læsere. Denne episode kunne ikke være blevet til uden støtte fra de 602 tilhængere:
Credits|2|True|Støt næste episode af Pepper&Carrot; hver en krone gør en forskel
Credits|3|False|Licens: Creative Commons Kreditering 4.0 Kildematerialet: tilgængelige på www.peppercarrot.com Værktøj: denne episode er 100% designet med fri software: Krita 2.9.9, Inkscape 0.91 på Linux Mint 17
Credits|4|False|https://www.patreon.com/davidrevoy
