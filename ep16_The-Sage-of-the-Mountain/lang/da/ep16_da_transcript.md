# Transcript of Pepper&Carrot Episode 16 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 16: Den Vise på bjerget

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Safran|9|False|Nej, nej, nej og nej, Pepper.
Skrift|2|True|Safrans
Skrift|3|False|Hekseri
Skrift|1|False|★★★
Skrift|5|False|SLAGTER
Skrift|6|False|15
Skrift|4|False|13
Skrift|7|False|Stjerne-vej
Skrift|8|False|Frisør
Lyd|10|True|Glug
Lyd|11|False|Glug

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Safran|2|False|Jeg leger ikke mægler, ikke engang for en veninde. Sådan noget ender altid galt.
Safran|1|True|Det er noget du selv må klare.
Pepper|3|False|Så er du sød Safran...
Pepper|4|True|Jeg lærer intet af mine gudmødre, de overtager mit hjem, beklager sig over deres gamle knogler og skælder mig ud, uanset hvad jeg gør...
Safran|6|True|Hør nu her, du er en heks. Opfør dig som en heks!
Safran|8|False|Som en stor pige!
Safran|7|True|Tal selv med dem!
Pepper|5|False|Kan du virkelig ikke hjælpe mig med at tale med dem?...
Safran|9|False|Okay, okay! Jeg kan vel godt tage med dig og tale med dem…

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Timian|1|False|Hvad mener du med, at du ikke lærer noget?!
Timian|12|False|Og du er ikke engang modig nok til at sige det selv?!
Timian|14|False|Hvis min ryg havde det bedre, ville jeg give dig en lærestreg! Og ikke kun en med hekseri, hvis du forstår hvad jeg mener!
Cayenne|15|True|Timian, stop.
Cayenne|16|False|De har ret.
Fugl|2|True|pip
Fugl|3|False|pip
Fugl|4|True|pip
Fugl|5|False|pip
Fugl|6|True|pip
Fugl|7|False|pip
Fugl|8|True|pip
Fugl|9|False|pip
Fugl|10|True|pip
Fugl|11|False|pip
Lyd|13|False|BAM !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|True|Vi er ikke helt unge længere, og det er det rette tidspunkt til en lektion.
Cayenne|2|True|Jeg foreslår en audiens hos
Timian|5|True|He he!
Timian|6|True|Den Vise?
Timian|7|False|Er de ikke for unge til at forstå det?
Pepper|8|True|For unge til hvad!?
Pepper|9|False|Lad os komme afsted!
Safran|10|False|"Os"?
Timian|11|True|Så, nu er vi her.
Timian|12|True|Gå op, så I kan stifte bekendtskab med Den Vise.
Timian|13|False|Han er på toppen af dette bjerg.
Timian|14|True|Er i klar til
Timian|15|False|en ordentlig lektion?
Cayenne|3|True|Den Vise på bjerget!
Cayenne|4|False|Hans råd er helt essentielle.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Klar!
Safran|2|False|Også mig!
Timian|3|False|Godt, godt! God indstilling!
Pepper|9|False|TIL ANGREB!!!
Safran|6|False|Et... ET MONSTER !
Pepper|8|True|DET ER EN FÆLDE !
Pepper|7|True|JEG VIDSTE DET!!!
Pepper|5|False|Åh nej!
Timian|4|False|Løb!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|GRAVITAS SPIRALIS !
Safran|2|False|BRASERO INTENSIA !
Lyd|3|False|Plask !
Timian|4|True|Tsk tsk, ungdommen nutildags, I vil altid angribe alt og alle!
Pepper|7|False|Endnu en åndssvag lektion!
Credits|11|False|Baseret på Hereva-universet skabt af David Revoy med bidrag af Craig Maloney. Rettelser af Willem Sonke, Moini, Hali, CGand og Alex Gryson. Licens: Creative Commons Kreditering 4.0, Værktøj: Krita, Blender, G'MIC, Inkscape på Linux Ubuntu
Credits|10|False|- SLUT -
Fortæller|8|False|04/2016 – www.peppercarrot.com – Tegning og manuskript: David Revoy – Dansk oversættelse: Emmiline Alapetite
Credits|9|False|Når det ikke går som man vil, er intet så godt som et dejligt bad! Det er godt for både vores gigt og for jeres nerver!...
Timian|6|False|Den Vises lektion er at efterligne ham!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot er helt gratis, open-source, og sponsoreret af læsere. Denne episode kunne ikke være blevet til uden støtte fra de 671 tilhængere:
Credits|2|False|Støt næste episode af Pepper&Carrot på www.patreon.com/davidrevoy
