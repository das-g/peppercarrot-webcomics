# Transcript of Pepper&Carrot Episode 17 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 17 : Un Nouveau Départ

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumin|1|False|Mais Pepper... ...Reviens...
Pepper|2|False|NON ! JE PARS !!
Pepper|3|False|Vous n'enseignez pas de vraie sorcellerie ! Je m'en vais !...chez les sorcières de Ah !
Son|4|False|Zo oo sh !|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Bon, rejoignons le pays des lunes couchantes.
Pepper|2|False|Shichimi nous conseillera pour entrer chez les sorcières de Ah.
Pepper|3|False|Carrot, sors la carte et la boussole : il y a trop de nuages et je ne sais pas où je vais.
Pepper|5|True|ZUT !
Pepper|6|True|Des turbulences !
Pepper|7|False|ACCROCHE-TOI !
Pepper|9|False|Oh non !!!
Son|8|False|BrrOoooOoo ! !|nowhitespace
Écriture|4|False|N

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Cr rAsh ! !|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monstre|6|False|Kaïe ! !
Monstre|5|True|Kaïe ! !
Pepper|1|False|Ne t'inquiète surtout pas, Carrot...
Cayenne|2|True|"Un bon regard noir évite bien des sorts inutiles !...
Pepper|7|False|Moi, j'aurais bien sûr préféré apprendre quelques bons sorts d'attaque ; mais bon...
Pepper|8|True|Zut... plus de balai ni d'affaires.
Pepper|9|False|La route va être longue...
Cayenne|3|False|...Mate-les. Domine-les !"

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumin|4|False|"Connaître les plantes comestibles ? C'est connaître des potions toutes prêtes contre la faim."
Pepper|6|False|M'en a-t-elle appris ne serait-ce qu'une seule ?
Pepper|2|True|Moi aussi je suis à bout de forces...
Pepper|3|False|Et ça fait des jours qu'on n'a rien mangé.
Carrot|1|False|Groo
Pepper|5|True|Mais de vraies potions ?...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thym|1|False|"Une vraie sorcière de Chaosah n'a besoin ni de cartes ni de boussoles sous un ciel étoilé."
Pepper|3|True|Courage Carrot !
Pepper|4|False|Regarde, on y est !
Pepper|2|False|... moi j'aurais préféré un vrai cours de divination !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|7|False|Licence : Creative Commons Attribution 4.0, Logiciels: Krita, G'MIC, Inkscape sur Ubuntu
Crédits|6|False|Basé sur l'univers d'Hereva créé par David Revoy avec les contributions de Craig Maloney. Corrections de Willem Sonke, Moini, Hali, CGand et Alex Gryson.
Narrateur|4|False|- FIN -
Crédits|5|False|06/2016 - www.peppercarrot.com - Dessin & Scénario : David Revoy
Pepper|2|False|Tu connais à présent toute l'histoire.
Shichimi|3|False|...et tu me dis que tu es ici car elles ne t'ont rien appris ?
Pepper|1|True|...et voilà.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|Pepper&Carrot est entièrement libre, open-source, et sponsorisé grâce au mécénat de ses lecteurs. Pour cet épisode, merci aux 719 Mécènes :
Crédits|2|False|Vous aussi, devenez mécène de Pepper&Carrot sur www.patreon.com/davidrevoy
