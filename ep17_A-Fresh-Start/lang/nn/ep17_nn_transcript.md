# Transcript of Pepper&Carrot Episode 17 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 17: Ein ny start

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Karve|1|False|Men Pepar ... Ikkje gå ...
Pepar|2|False|NEI! EG DREG!
Pepar|3|False|De lærer meg ikkje ekte trolldom! Eg dreg til ah-heksene!
Lyd|4|False|S v o s j !|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|4|True|N
Pepar|1|True|Og så går turen til landet med månenedgangane.
Pepar|2|False|Me kan høyra med Shichimi om korleis me kan verta ah-hekser.
Pepar|3|False|Hent fram kart og kompass, Gulrot. Eg ser ingenting med alle desse skyene her!
Pepar|5|True|SØREN!
Pepar|6|True|Turbulens!
Pepar|7|False|HALD DEG FAST!
Pepar|9|False|Å nei!!!
Lyd|8|False|BrrOoooOoo! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Kr rasj !|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|6|False|Aah!
Monster|5|True|Wah!
Pepar|1|False|Det viktige no er å ikkje få panikk, Gulrot ...
Kajenne|2|True|«Ein god, mørk stir er verd ti ubrukelege forbanningar!
Pepar|7|False|Sjølv ville eg føretrekt å læra nokre gode åtaksformlar ... Men greitt ...
Pepar|8|True|Søren ... Utan sopelime og utstyr ...
Pepar|9|False|Dette vert ein lang tur ...
Kajenne|3|False|Stir dei i senk ... Få makt over dei!»

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|5|True|Men ekte trylledrikkar?!
Karve|4|False|«Å kjenna til kva planter som er etande, er som å veta korleis ein finn fiks ferdige trylledrikkar mot svolt!»
Pepar|6|False|Om ho berre hadde lært meg å laga ein einaste ein!
Pepar|2|True|Eg òg er heilt utsliten, Gulrot ...
Pepar|3|False|Me har ikkje ete noko på fleire dagar!
Gulrot|1|False|Rumle

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Timian|1|False|«Ei ekte kaosah-heks treng ikkje kart eller kompass; det held med ein stjerneklår himmel.»
Pepar|3|True|Opp med humøret, Gulrot!
Pepar|4|False|Me er framme!
Pepar|2|False|... Men eg ville likevel føretrekt eit ekte kurs i spådom!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|7|False|Lisens: Creative Commons Attribution 4.0. Programvare: Krita, G’MIC og Inkscape på Ubuntu.
Bidragsytarar|6|False|Bygd på Hereva-universet skapa av David Revoy, med bidrag frå Craig Maloney og rettingar frå Willem Sonke, Moini, Hali, CGand og Alex Gryson.
Forteljar|4|False|– SLUTT –
Bidragsytarar|5|False|Juni 2016 – www.peppercarrot.com – Teikna og fortald av David Revoy – Omsett av Karl Ove Hufthammer og Arild Torvund Olsen
Pepar|2|False|... no veit du alt som hende.
Shichimi|3|False|... og du seier at du er komen fordi dei ikkje har lært deg noko som helst ?!
Pepar|1|True|Sånn, ...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane. Takk til dei 719 som støtta denne episoden:
Bidragsytarar|2|False|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot: www.patreon.com/davidrevoy
