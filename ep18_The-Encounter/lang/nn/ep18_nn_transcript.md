# Transcript of Pepper&Carrot Episode 18 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 18: Det fyrste møtet

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|"Hippiah" subtle on the box
Pepar|1|False|OK, no kan du sjå!
Pepar|2|False|TA~DAA~~A !|nowhitespace
Pepar|3|True|Ekte hippiah-hekseklede!
Pepar|4|False|Det var det einaste dei hadde att i butikken, så eg tok dei medan eg ventar på dei nye kleda mine.
Pepar|7|False|Vel? Minner dei deg ikkje om noko? ...
Skrift|6|False|HIPPIAH

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Wood panel
<hidden>|0|False|Door
Skrift|1|False|Skule for hippiah-hekseri
Skrift|3|False|KJØKEN
Gulrot|2|False|Rumle
Basilik|4|False|Bra, Oregano!
Basilik|5|False|Bra, Kardemomme!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Basilik|1|False|Veldig bra, Kanel!
Basilik|2|False|Bra, Kamille!
Basilik|3|False|Pepar?!
Pepar|4|False|Men eg ...
Basilik|5|True|Mislukka igjen!!!
Basilik|6|False|Må eg minna deg på at hippiah ikkje er magi for å laga ugrasmiddel?
Oregano|7|False|HA, HA, HA!
Kardemomme|8|False|HA, HA!
Kanel|9|False|HA, HA!
Kamille|10|False|HA, HA, HA!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Oregano|1|False|HA, HA, HA!
Kardemomme|2|False|Ho-ho, ho!
Kanel|9|False|Pfff!
Kamille|10|False|HA, HA, HA!
Pepar|3|True|SLUTT
Pepar|4|True|!!!|nowhitespace
Pepar|5|True|Det er ikkje min feil!!!
Pepar|6|True|Kom igjen ...
Pepar|7|True|SLUTT
Pepar|8|False|!!!|nowhitespace
Kardemomme|12|False|HA, HA-HA, HA!
Oregano|11|False|Ho, ho, ho!
Kamille|14|False|HA-HA, HA, HA!
Kanel|15|False|HA-HA, HA!
Pepar|13|False|Grrr... Slutt ... Eg sa ...
Pepar|16|True|Eg sa ...
Pepar|17|False|SLUTT!!!
Lyd|18|False|KABOOOM! ! !|nowhitespace
Lyd|19|False|k n u s !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|on the door
Kajenne|1|True|Kremt ...
Kajenne|2|False|Om du vil, kan me overta henne ...
Kajenne|3|False|Det har seg slik at me er på leit etter ein kaosah-elev ...
Timian|4|False|... og ho der ser ut til å vera ein perfekt kandidat.
Basilik|5|True|Kajenne?!
Basilik|6|True|Timian?! Karve?!
Basilik|7|False|Men eg trudde de ... det er umogleg!
Skrift|8|False|KJØKEN

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Svipp!|nowhitespace
Lyd|2|False|Dunk!|nowhitespace
Pepar|3|False|Tji-hi!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|"Hippiah" subtle on the box
<hidden>|0|False|Don't translate the names of the scenarios
Pepar|1|False|Noko seier meg at me to skal verta gode vener!
Pepar|2|False|... og eg trur eg alt veit kva namn eg vil gje deg!
Pepar|3|False|Vel? Kva minner det deg om?
Gulrot|4|False|Rumle?
Skrift|5|False|HIPPIAH
Forteljar|6|False|– SLUTT –
Bidragsytarar|7|False|August 2016 – www.peppercarrot.com – Teikna og fortald av David Revoy – Omsett av Arild Torvund Olsen og Karl Ove Hufthammer
Bidragsytarar|8|False|Handlinga er inspirert av to scenario føreslegne av Craig Maloney: «You found me, I Choose You» og «Visit from Hippiah».
Bidragsytarar|9|False|Bygd på Hereva-universet skapa av David Revoy, med bidrag frå Craig Maloney og rettingar frå Willem Sonke, Moini, Hali, CGand og Alex Gryson.
Bidragsytarar|10|False|Lisens: Creative Commons Attribution 4.0. Programvare: Krita 3.0 og Inkscape 0.91 på Manjaro XFCE.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane. Takk til dei 720 som støtta denne episoden:
Bidragsytarar|2|False|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot: www.patreon.com/davidrevoy
