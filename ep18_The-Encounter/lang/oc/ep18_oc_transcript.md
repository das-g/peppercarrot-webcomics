# Transcript of Pepper&Carrot Episode 18 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 18 : Lo Rencontre

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|"Hippiah" subtle on the box
Pepper|1|False|Es bon, pòs agachar !
Pepper|2|True|TA~DÀÀ~~À !|nowhitespace
Pepper|3|True|Una tenguda de masca d'Hippiah !
Pepper|4|False|I aviá pas pus qu'aquò dins la botiga, alara l'ai presa, en esperant que mos vestits novèls arriben !
Pepper|7|False|Alara, te fa pas pensar a quicòm ?...
Escritura|6|False|HIPPIAH

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Wood panel
<hidden>|0|False|Door
Escritura|1|False|Escòla de mascas Hippiah
Escritura|3|False|COSINAS
Carròt|2|False|Groo
Baselic|4|False|Romanin, plan.
Baselic|5|False|Cardamòma, plan.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Baselic|1|False|Canèla, fòrça plan.
Baselic|2|False|Camomilha, plan.
Baselic|3|False|Pepper ?!
Pepper|4|False|Mas...
Baselic|5|True|Mancat tornar !!!
Baselic|6|False|Vos rapèli que Hippiah es pas una magia per far de deserbants !...
Romanin|7|False|HA HAHA HA !
Cardamòma|8|False|HA HA !
Canèla|9|False|HA HA !
Camomilha|10|False|HA HAHA HA !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Romanin|1|False|HAHA HA !
Cardamòma|2|False|HoHo Ho !
Canèla|9|False|Pfff !
Camomilha|10|False|HAHA HA HA !
Pepper|3|True|ARRESTATZ
Pepper|4|True|!!!
Pepper|5|True|Es pas ma fauta !!!
Pepper|6|True|Mas...
Pepper|7|True|N'I A PRO
Pepper|8|False|!!!
Cardamòma|12|False|HAHA HA HA !
Romanin|11|False|HoHo Ho !
Camomilha|14|False|HAHA HA HA !
Canèla|15|False|HAHA HA !!
Pepper|13|False|Gnn... Arrestatz.. Arres...
Pepper|16|True|Ai dit ...
Pepper|17|False|PRO !!!
Son|18|False|B A AAM ! ! !|nowhitespace
Son|19|False|C R AAC ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|on the door
Cayenne|1|True|Hem Hem...
Cayenne|2|False|Se volètz, nos podèm ocupar d'ela...
Cayenne|3|False|Tomba plan, cercàvem una escolana a Caosah...
Frigola|4|False|...e aquesta sembla èstre la candidata ideala.
Baselic|5|True|Cayenne ?!
Baselic|6|True|Frigola ?! Comin ?!
Baselic|7|False|Mas pensavi qu'èretz totas... ...es pas possible !
Escritura|8|False|COSINAS

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|fuiiip !|nowhitespace
Son|2|False|Clomp !|nowhitespace
Pepper|3|False|Hi hi hi !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|"Hippiah" subtle on the box
<hidden>|0|False|Don't translate the names of the scenarios
Pepper|1|True|Ai a l'idèa que nos endevenirem plan, tu e ieu !
Pepper|2|False|...e sabi ja cossí te vau nomenar !
Pepper|3|False|Alara, alara ! A qué te fa pensar ?!
Carròt|4|False|Groo?
Escritura|5|False|HIPPIAH
Narrator|6|False|- FIN -
Crèdits|7|False|08/2016 - www.peppercarrot.com - Dessenh & Scenari : David Revoy
Crèdits|8|False|Scenaris inspirats de dos scenaris prepausats per Craig Maloney : « You found me, I Choose You » e « Visit from Hippiah ».
Crèdits|9|False|Basat sus l'univèrs d'Hereva creat per David Revoy amb las contribucions de Craig Maloney. Correccions de Willem Sonke, Moini, Hali, CGand e Alex Gryson.
Crèdits|10|False|Licéncia : Creative Commons Attribution 4.0, Logicials : Krita 3.0, Inkscape 0.91 sus Manjaro XFCE

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|1|False|Pepper&Carrot es completament liure, open source, e esponsorizat mercés al mecenat de sos lectors. Per aqueste episòdi, mercé als 720 mecènas :
Crèdits|2|False|Vos tanben, venètz mecèna de Pepper&Carrot sus www.patreon.com/davidrevoy
