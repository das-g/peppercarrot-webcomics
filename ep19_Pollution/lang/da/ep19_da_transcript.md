# Transcript of Pepper&Carrot Episode 19 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 19: Forurening

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|True|Det nytter ikke at holde øje med dig længere...
Cayenne|2|True|Grav alle disse mislykkede eliksirer ned så hurtigt som muligt, så du kan komme hjem og hvile dig
Cayenne|3|False|At se dig lykkes med én i morgen er nok bare ønsketænkning, men man ved jo aldrig!
Cayenne|4|False|Ja ja, jeg skynder mig!
Pepper|5|True|Men hvad er det egentlig også det der med altid at grave alt ned ?
Pepper|6|True|Ville det ikke være bedre hvis…
Pepper|7|False|hvis HVAD ?
Cayenne|8|False|Jeg går hjem.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|labels in the garden :-)
Pepper|1|True|Øhm…
Pepper|2|True|Altså jeg er ikke ekspert…
Pepper|3|True|Men vores køkkenhave er ret mærkelig i år,
Pepper|4|False|og det gælder også mange af planterne rundt om huset.
Skrift|5|False|Og myrerne laver virkelig underlige ting.
Skrift|6|False|Så, altså...
Pepper|7|False|Jeg tænker, at vi måske har et forurenings-problem, og at vi bør overveje at rense lidt op...
Pepper|8|True|Hør, frøken Jeg-fejler-med-alle-mine-eliksirer,
Pepper|9|False|I Kaosah begraver vi vores fiaskoer!
Cayenne|10|True|Det har været traditionen siden tidernes morgen, og vi er ligeglade med MODER JORD!
Cayenne|11|False|Så ti stille og GRAV !!
Cayenne|12|True|din Hippiah-heksedragt stiger dig tydeligvis til hovedet!
Cayenne|13|True|Tomater
Cayenne|14|False|Auberginer

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|vi begraver vores fiaskoer
Pepper|2|False|siden tidernes morgen
Pepper|3|False|traditionen
Pepper|4|False|Nåh JA!
Carrot|5|False|Kom så, hurtigere, Carrot!
Pepper|6|False|Zzzz

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Diary label on book
Skrift|6|False|Cayennes Dagbog
Pepper|1|False|"Åh, tapre blonde kriger med det gyldne hår!"
Pepper|2|False|"...Du, som hjemsøger mit kaos!"
Pepper|3|False|"...Du er entropien af mit daggry."
Pepper|4|True|Det er utroligt, hvad man kan lære af hekse der begraver deres fiaskoer!
Pepper|5|False|Fin poesi, Cayenne!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|True|Vi har alle haft vores fiaskoer.
Cayenne|2|True|Jeg er ikke længere den Cayenne, der skrev det.
Cayenne|3|False|Den dagbog blev gravet ned af en helt bestemt grund.
Pepper|4|False|...
Pepper|5|True|Okay!
Pepper|6|False|Men hvad siger I til denne her?
Skrift|7|False|Det er bare et ungdomsfejltrin... Og det er i hvert fald ikke til din alder!
Timian|8|False|Hm...
Pepper|9|True|Jeg forstår...
Pepper|10|False|I er ikke som sådan flove over alt det her...
Pepper|11|False|KAOSAH SUTRA af Timian

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Men miljøet ! Naturen !
Pepper|2|False|Det har aldrig givet os problemer ind til videre!
Cayenne|3|True|DYBT NED!
Cayenne|4|True|Man kan ikke bare forurene det hele uden at det får konsekvenser! !!!
Cayenne|5|True|Vi er Kaosah-hekse! Og vi begraver vores problemer
Cayenne|6|False|Traditionerne er ikke til diskussion!
Spidskommen|7|True|Orv, se lige hvad jeg fandt!
Spidskommen|8|False|Jeg kan ikke tro det!
Spidskommen|9|False|Hvordan kan den være havnet her?
Spidskommen|10|False|Den skal uden tvivl stemmes, men den lyder stadig fint.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Label of materials to translate
Spidskommen|1|False|glas
Spidskommen|2|True|metal
Spidskommen|3|False|Hihi, jeg har glemt teksten!
Cayenne|4|True|Men min sangbog må være her et sted...
Cayenne|5|True|Hvordan var det nu? Ka~Ka Ka, Kaooosah !
Cayenne|6|True|...så vi er alle enige. Opdatering af Kaosah-reglerne:
Cayenne|7|True|fra nu af, så
Cayenne|8|True|sorterer,
Cayenne|9|False|knuser
Skrift|10|False|og genbruger vi alt!
Skrift|11|False|ALT!!!
Fortæller|12|False|- SLUT -
Credits|13|False|09/2016 - www.peppercarrot.com – Tegning og manuskript: David Revoy – Dansk oversættelse: Emmiline Alapetite
Credits|14|False|Script doctor: Craig Maloney. Genlæsning og hjælp til dialogerne: Valvin, Seblediacre, Alex Gryson. Inspiration: "The book of secrets" af Juan José Segura
Credits|15|False|Baseret på Hereva-universet skabt af David Revoy med bidrag af Craig Maloney. Rettelser af Willem Sonke, Moini, Hali, CGand et Alex Gryson.
Credits|16|False|Licens: Creative Commons Attribution 4.0, Værktøj: Krita 3.0.1, Inkscape 0.91 på Arch Linux XFCE

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot er helt gratis, open-source, og sponsoreret af læsere. Denne episode kunne ikke være blevet til uden støtte fra de 755 tilhængere:
Credits|2|False|Støt næste episode af Pepper&Carrot på www.patreon.com/davidrevoy
