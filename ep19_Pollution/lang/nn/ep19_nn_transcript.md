# Transcript of Pepper&Carrot Episode 19 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 19: Forureining

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kajenne|1|True|Eg stikk inn.
Kajenne|2|True|Det har lite for seg å stå og glo på at du grev ...
Kajenne|3|False|Grav ned alle desse mislukka trylledrikkane, og kom inn for å kvila så snart du kan.
Kajenne|4|False|Det er nok berre ynskjetenking at du vil lukkast betre i morgon, men me får vel halda håpet i live!
Pepar|5|True|Ja då, eg skal skunda meg!
Pepar|6|True|Men kva er greia med at me alltid skal grava ned ting ?
Pepar|7|False|Var det ikkje betre om me ...
Kajenne|8|False|Om me KVA ?

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|labels in the garden :-)
Pepar|1|True|Æh ...
Pepar|2|True|Eg er jo ikkje akkurat nokon ekspert,
Pepar|3|True|men kjøkenhagen vår er jo ganske merkeleg i år.
Pepar|4|False|Og det gjeld for så vidt mange av dei andre plantene her omkring òg.
Skrift|5|False|Tomat
Skrift|6|False|Aubergine
Pepar|7|False|Og maurane – dei har visst noko underleg på gang.
Pepar|8|True|Så ...
Pepar|9|False|... eg tenkte at me kanskje har eit lite forureiningsproblem – og kanskje burde reinska opp litt ...
Kajenne|10|True|Høyr her, frøken « eg-øydelegg-alle-trylledrikkane-mine »:
Kajenne|11|False|Det ser ut til at hippiah-heksedrakta har gått deg til hovudet.
Kajenne|12|True|I kaosah-magien gravlegg me alltid feila våre!
Kajenne|13|False|Det har vore tradisjon i all tid, og eg gjev regelrett blaffen i kva MODER JORD måtte meina om den saka!
Kajenne|14|False|Så klapp igjen og GRAV !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|... me gravlegg feila våre ...
Pepar|2|False|... tradisjon ...
Pepar|3|False|... i all tid ...
Pepar|4|False|SJØLVSAGT!
Gulrot|5|False|Zzzz
Pepar|6|False|Kom igjen! Raska på, Gulrot!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Diary label on book
Skrift|6|False|Kajennes private dagbok
Pepar|1|False|«Å, fagre, blonde krigar med gylne lokkar!»
Pepar|2|False|«Du som heimsøkjer kaoset mitt!»
Pepar|3|False|«Du er entropien i solrenninga mi.»
Pepar|4|True|Vakker poesi, frøken Kajenne!
Pepar|5|False|Det er utruleg kva ein kan finna ut om hekser som grav-legg alle feila sine!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kajenne|1|True|Me har alle gjort feil.
Kajenne|2|True|Eg er ikkje lenger den Kajenne som skreiv det der.
Kajenne|3|False|Det er ein god grunn til at dagboka er graven ned.
Pepar|4|False|...
Pepar|5|True|OK!
Pepar|6|False|Men kva har de å seia til dette?!
Skrift|7|False|KAOSAH-SUTRA av Timian
Timian|8|False|Det er berre eit feiltrinn frå ungdomstida – og uansett er det ikkje noko for jenter på din alder!
Pepar|9|True|Hm ...
Pepar|10|False|Eg forstår ...
Pepar|11|False|De er eigentleg ikkje flaue over noko av dette ...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|Men kva med miljøe t ? Naturen !
Pepar|2|False|Me kan ikkje halda fram med å forureina slik utan at det får alvorlege følgjer!!!
Kajenne|3|False|Det har ikkje vore noko problem til no!
Kajenne|4|True|Me er kaosah-hekser, og problema våre gravlegg me alltid
Kajenne|5|True|djupt nede!
Kajenne|6|False|Tradisjonar kan ikkje diskuterast!
Karve|7|True|Å, sjå kva eg fann!
Karve|8|False|Eg kan knapt tru det!
Karve|9|False|Korleis hamna denne her?
Karve|10|False|Treng nok litt finstemming, men det er enno lyd i han.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Label of materials to translate
Karve|1|False|Korleis var han no igjen? Ka~ka, ka, kaooosah!
Karve|2|True|Ha-ha, eg har visst gløymt teksten!
Karve|3|False|Kanskje songboka mi òg er her ein plass ...
Kajenne|4|True|... så då er det einstemmig? Oppdatering av kaosah-reglane:
Kajenne|5|True|Frå no av
Kajenne|6|True|sorterer,
Kajenne|7|True|knuser
Kajenne|8|True|og resirkulerer me alt!
Kajenne|9|False|ABSOLUTT ALT!
Skrift|10|False|glas
Skrift|11|False|metall
Forteljar|12|False|– SLUTT –
Bidragsytarar|13|False|September 2016 – www.peppercarrot.com – Teikna og fortald av David Revoy – Omsett av Karl Ove Hufthammer og Arild Torvund Olsen
Bidragsytarar|14|False|Manuskriptdokter: Craig Maloney. Korrekturlesing og dialoghjelp: Valvin, Seblediacre og Alex Gryson. Inspirasjon: «The book of secrets» av Juan José Segura.
Bidragsytarar|15|False|Bygd på Hereva-universet skapa av David Revoy, med bidrag frå Craig Maloney og rettingar frå Willem Sonke, Moini, Hali, CGand og Alex Gryson.
Bidragsytarar|16|False|Lisens: Creative Commons Attribution 4.0. Programvare: Krita 3.0.1 og Inkscape 0.91 på Arch Linux XFCE.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane. Takk til dei 755 som støtta denne episoden:
Bidragsytarar|2|False|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot: www.patreon.com/davidrevoy
