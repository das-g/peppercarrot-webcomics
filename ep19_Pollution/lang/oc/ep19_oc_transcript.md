# Transcript of Pepper&Carrot Episode 19 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 19 : Pollucion

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|2|True|Pas besonh de te susvelhar mai...
Cayenne|3|False|Entèrra-me totas aquelas pocions mancadas e dintra lèu per te pausar.
Cayenne|4|False|Te ne veire capitar una deman passarà benlèu l'estadi de pantais, qual sap !
Pepper|5|True|Es bon, m'afani !
Pepper|6|True|E puèi, de qu'es aquela tissa de voler totjorn tot enterrar ?
Pepper|7|False|Seriá pas melhor se...
Cayenne|8|False|se QUÉ ?
Cayenne|1|True|Dintri.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|labels in the garden :-)
Pepper|1|True|Èèèè...
Pepper|2|True|Rai, soi pas expèrta...
Pepper|3|True|Mas ongan, nòstre òrt es subre-estranh
Pepper|4|False|e pertòca tanben un bon nombre de plantas a l'entorn de l'ostal...
Pepper|7|False|Per çò qu'es de las formigas, fan de causas fòrça estranhas.
Pepper|8|True|Alara, bon...
Pepper|9|False|Me soi pensat, avèm benlèu un problèma de pollucion, e calriá segurament netejar tot aquò...
Cayenne|10|True|Escota, madomaisèla Manqui-Totas-Mas-Pocions,
Cayenne|12|True|A Caosah, enterram nòstras malescadudas !
Cayenne|13|False|Es la tradicion dempuèi las tenèbras del temps e ne nos batèm de çò que ne pensa DÒNA NATURA !
Cayenne|14|False|Alara te calas e CAVAS !!
Cayenne|11|False|ta tenguda d'Hippiah te deu segurament conflar lo cap !
Escritura|5|False|Tomatas
Escritura|6|False|Auberginas

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|enterram nòstras malescadudas
Pepper|3|False|dempuèi las tenèbras dels temps
Pepper|2|False|la tradicion
Pepper|4|False|Mas ÒC !
Pepper|6|False|Anem, mai lèu, Carròt !
Carròt|5|False|Zzzz

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Diary label on book
Escritura|6|False|Jornal Intime de Cayenne
Pepper|1|False|« Ò, pros guerrièr polit als pels blonds ! »
Pepper|2|False|« ...Tu que trevas mon caòs ! »
Pepper|3|False|« ...Siás l'entropia de mas albors. »
Pepper|4|True|Es fòl tot çò que se pòt apréner de las mascas qu'entèrran totas lors malescadudas !
Pepper|5|False|Polida poesia, mèstra Cayenne !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|True|Avèm totas agut de malescadudas.
Cayenne|2|True|Soi pas pus la Cayenne qu'escriguèt aquò.
Cayenne|3|False|Aqueste jornal foguèt enterrat per una rason precisa.
Pepper|4|False|...
Pepper|5|True|D'acòrdi !
Pepper|6|False|Mas que disètz d'aquò, e ?
Frigola|8|False|Es pas qu'un desvari de jovença... A mai, es pas per qualqu'un tan jove coma tu !
Pepper|9|True|Mm...
Pepper|10|True|Vesi...
Pepper|11|False|Sètz pas vertadièrament entorteligadas per tot aquò...
Escritura|7|False|CAOSAH SUTRA per Frigola

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Mas l'environament ! La natura !
Cayenne|3|False|Nos a pas pausat cap de problèma fins ara !
Cayenne|5|True|PRIGONDAMENT !
Pepper|2|False|Podèm pas contunhar de tot polluir atal sens patir de consequéncias grèvas !!!
Cayenne|4|True|Sèm de mascas de Caosah ! E nòstres problèmas, los enterram
Cayenne|6|False|Cal pas debatre de las tradicions !
Comin|7|True|Òu, gaitatz çò que veni de retrobar !
Comin|8|False|Incredible !
Comin|9|False|Cossí a pogut arribar aquí ?
Comin|10|False|A segurament besonh d'un petit acordatge, mas sona totjorn.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Label of materials to translate
Escritura|10|False|veire
Escritura|11|False|metal
Comin|2|True|Ha ha, ai doblidat las paraulas !
Comin|3|False|Probable que mon libre de cants es per aquí...
Comin|1|False|Cossi èra, ja ? Ca~Ca Ca, Caooosah !
Cayenne|4|True|...doncas, sèm totas d'acòrdi. Actualizacion de las règlas de Caosah :
Cayenne|5|True|tre ara,
Cayenne|6|True|triam,
Cayenne|7|True|trissam
Cayenne|8|True|e reciclam tot !
Cayenne|9|False|TOT !!!
Narrator|12|False|- FIN -
Crèdits|13|False|09/2016 - www.peppercarrot.com - Dessenh & Scenari : David Revoy
Crèdits|14|False|Script doctor : Craig Maloney. Relectura e ajuda als dialògues : Valvin, Seblediacre e Alex Gryson. Inspiracion : « The book of secrets » de Juan José Segura
Crèdits|15|False|Basat sus l'univèrs d'Hereva creat per David Revoy amb las contribucions de Craig Maloney. Correccions de Willem Sonke, Moini, Hali, CGand e Alex Gryson.
Crèdits|16|False|Licéncia : Creative Commons Attribution 4.0, Logicials : Krita 3.0.1, Inkscape 0.91 sus Arch Linux XFCE

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|1|False|Pepper&Carrot es completament liure, open source, e esponsorizat mercés al mecenat de sos lectors. Per aqueste episòdi, mercé als 755 mecènas :
Crèdits|2|False|Vos tanben, venètz mecèna de Pepper&Carrot sus www.patreon.com/davidrevoy
