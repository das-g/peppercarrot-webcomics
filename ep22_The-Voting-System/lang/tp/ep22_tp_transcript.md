# Transcript of Pepper&Carrot Episode 22 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa mute tu: ilo pi nasin wile

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan lawa pi ma Komona|1|False|pona la, utala musi pi wawa nasa li ken open!
sitelen|2|False|utala
sitelen|3|False|pi wawa
sitelen|4|False|nasa

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan lawa pi ma Komona|1|False|jan pali mi pi sona ilo la, sina ale kin li ken pali lon tenpo ni!
jan lawa pi ma Komona|2|False|o lukin, jan pona o. o lukin e ilo pona pi ma Komona a!
jan lawa pi ma Komona|3|False|nena laso la, nanpa li tawa jan utala. nena loje la, nanpa li weka tan jan utala. sina pana e ni!
jan lawa pi ma Komona|4|True|"taso kulupu lawa pi utala musi li pali e seme?"
jan lawa pi ma Komona|5|False|o sona e ni: nasin pona li tawa ona tan mi!
jan lawa pi ma Komona|6|False|jan pi kulupu lawa li jo e ilo namako. nena la, ona li ken pana mute e nanpa, li ken weka mute e nanpa!
jan Pepa|7|False|pona a!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan lawa pi ma Komona|5|False|…mani Komo 50,000!
jan lawa pi ma Komona|1|False|namako la— nanpa li kama lon sewi ona!
jan lawa pi ma Komona|3|False|nanpa pi jan utala li suli la, ona tu wan taso li ken awen lon utala musi!
jan lawa pi ma Komona|4|True|sina sona e ni: jan utala nanpa wan li kama jo e…
sitelen|2|False|1337
<hidden>|0|False|Edit this one, all others are linked
kalama jan|6|False|Luka

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
soweli Kawa|2|False|Nena
jan Pepa|3|False|nasin ni li pona, anu seme, soweli Kawa o?
jan Pepa|4|True|ona li sona pona a…
jan Pepa|5|True|li musi a…
jan Pepa|6|True|li kepeken wile pi jan ale a…
jan Pepa|7|False|… ona li nasin pi pona suli a!
jan Pepa|8|True|jan li ken sona e pona tan jan sona ala!
jan Pepa|9|False|suli li kama tan ni! tenpo ilo ni li tenpo suli!
jan lawa pi ma Komona|10|False|sina ale li jo ala jo e ilo?
kalama jan|11|False|pona!
kalama jan|12|False|jo!
kalama jan|13|False|joo!
kalama jan|14|False|jo!
kalama jan|15|False|joo!
jan lawa pi ma Komona|16|True|pona!
jan lawa pi ma Komona|17|True|utala musi li…
jan lawa pi ma Komona|18|False|OPEN!!
<hidden>|0|False|Edit this one, all others are linked
kalama jan|1|False|Luka

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan lawa pi ma Komona|1|False|jan Kamomile li open pali!
kalama|2|False|Sssuno…
jan Kamomile|3|False|KASITANMA!
kalama|4|False|Pakala!
kalama jan|5|True|Nena
kalama jan|6|True|Nena
kalama jan|7|True|Nena
kalama jan|8|True|Nena
kalama jan|9|True|Nena
kalama jan|10|True|Nena
kalama jan|11|False|Nena

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan lawa pi ma Komona|2|False|nanpa pi jan Kamomile li suli pona! tenpo ni la, jan Sisimi li pali!
sitelen|1|False|5861
jan Sisimi|4|False|SUNOS…
kalama|5|False|Wa lll oo!!|nowhitespace
jan Sisimi|6|False|SULIMUS!
kalama jan|7|False|aaa!!
kalama jan|8|False|ike!!
kalama jan|9|False|oko mi a!!!
jan Pepa|12|False|soweli Kawa o… ona li jan pona mi... tan ni la, o pana e nena pona tawa ona
soweli Kawa|13|False|Nena
kalama jan|10|True|iike!
kalama jan|11|False|ike aaa!
kalama jan|14|True|iiiike!
kalama jan|15|False|ike a!
jan lawa pi ma Komona|17|False|a, lukin la, jan lukin li wile lukin ala e pali suno ni! tenpo pi jan Pilulina li kama!
sitelen|16|False|-42
kalama jan|18|True|ike aaa!
kalama jan|19|True|iiike!
kalama jan|20|True|ike a!
kalama jan|21|False|ike!
<hidden>|0|False|Edit this one, all others are linked
kalama jan|3|False|Luka

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pilulina|1|False|PANAJUS KALANOKA!
kalama|2|False|Taw wa!|nowhitespace
kalama|3|False|Ttte l llo !|nowhitespace
jan lawa pi ma Komona|5|False|pona a! suli a! wawa a! jan Pilulina li kama jan nanpa wan lon tenpo ni! jan Kowijana o, pali kin!
sitelen|4|False|6225
jan Pilulina & kala Tulijan|6|False|Tap
jan Kowijana|8|False|MOLINA AWENTAWA!
kalama|9|False|Kaaa mmaa !|nowhitespace
jan lawa pi ma Komona|11|False|a, ken la, tenpo ni li tenpo pona ala tawa kiwen moli… jan Sapon o kama pali!
sitelen|10|False|2023
<hidden>|0|False|Edit this one, all others are linked
kalama jan|7|False|Luka

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Sapon|1|False|pakala! jan Pilulina li ken ala anpa e mi! mi pana e wawa ale mi!
jan Sapon|2|False|o weka, soweli Tupe o!
kalama|3|False|Sss suno !|nowhitespace
kalama|4|False|Ss ssuno !|nowhitespace
kalama|5|False|Koo!
soweli Tupe|6|False|mu!
jan Sapon|7|False|SIKELIJUS
jan Sapon|8|False|SELAAaaaaaa!|nowhitespace
kalama|9|False|Kk oonnn nnn !|nowhitespace
kalama|10|False|Tawwwwa!
kalama|11|False|Ss seli !|nowhitespace
kalama|12|False|Anpa!
jan Sapon|13|False|?!!
kalama|14|False|Ss seli !|nowhitespace
kalama|15|False|Ss seli !|nowhitespace
jan Sapon|16|False|ike aaa! !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Sapon|1|False|a… mi pilin ike tan ni!
jan Sapon|3|False|?!
sitelen|4|True|14
sitelen|5|False|849
jan Pepa|6|False|Nena
jan lawa Asiwepa|7|True|Nena
jan lawa Asiwepa|8|True|Nena
jan lawa Asiwepa|9|False|Nena
sitelen|10|True|18
sitelen|11|False|231
jan lawa pi ma Komona|12|True|o pona... o pona, jan ale o!
jan lawa pi ma Komona|13|False|jan Sisimi en jan Kowijana li kama weka!
jan lawa pi ma Komona|14|False|jan Kamomile en jan Pilulina en jan Sapon li awen lon utala musi!
<hidden>|0|False|Edit this one, all others are linked
kalama jan|2|False|Luka

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan lawa pi ma Komona|1|False|jan o, sina wile ala wile utala sin?
jan lawa pi ma Komona|2|False|o pali!
sitelen|3|True|1 5|nowhitespace
sitelen|4|False|703
sitelen|5|True|19
sitelen|6|False|863
sitelen|7|True|1 3|nowhitespace
sitelen|8|False|614
kalama|10|False|Anpa!
jan Pepa|11|False|mi ante e pilin mi tawa nasin ni...
jan lawa Asiwepa|12|True|Nena
jan lawa Asiwepa|13|True|Nena
jan lawa Asiwepa|14|False|Nena
sitelen toki|15|False|- PINI -
<hidden>|0|False|Edit this one, all others are linked
kalama jan|9|False|Luka

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|tenpo 05/2017 - www.peppercarrot.com - musi sitelen & toki li tan jan David Revoy - ante toki pi toki pona li tan jan Ret Samys
mama|2|False|jan Nicolas Artance en jan Valvin en jan Craig Maloney li pona e toki jan.
mama|3|False|musi ni li tan pali pi ma Elewa. pali ni li tan jan David Revoy. jan Craig Maloney li pana e pona tawa ona. jan Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson li pona e musi ni.
mama|4|False|ilo : ilo Krita 3.1.3 en ilo Inkscape 0.92.1 li kepeken ilo Linux Mint 18.1
mama|5|False|nasin pi lawa jo: Creative Commons Attribution 4.0
mama|6|False|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 864 li pana e mani la, lipu ni li lon:
mama|7|False|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa lon www.patreon.com/davidrevoy
