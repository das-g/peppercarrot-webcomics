# Transcript of Pepper&Carrot Episode 24 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa mute tu tu: kasi suli pi kama wan

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|5|False|ANPA !|nowhitespace
jan Kajen|1|True|sina wile e kasi pi kama wan
jan Kajen|4|False|nasin wawa mi li nasin pi pimeja pakala. mi pali ALA e ni.
jan Kajen|6|False|sina o kama sona e nimi wawa pakala!
jan Pepa|7|True|tenpo ni la...
jan Pepa|8|True|wawa pakala anu seme?
jan Pepa|9|False|mi ken ala kama sona e wawa pona tan seme?
jan Kajen|10|False|tenpo lili la, kulupu pi mun tu wan li kama. utala sona sina li kama kin. ni la, o kama sona! o awen e sona ! O SULI E SONA!!! O NASA ALA E LAWA SINA KEPEKEN NASIN ANTE NI!!!
jan Kajen|2|False|lon tomo ni anu seme?!?
jan Kajen|3|True|ALA A!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|False|Kalama!|nowhitespace
jan Pepa|5|True|kasi pi kama wan li weka tan tenpo lete anu seme?!
jan Pepa|7|False|mi ken pali e kasi suli mi kepeken wawa pi mi taso!
jan Pepa|8|True|ken la, mi ken pali e sin. mi ken wan e nasin Piponaa e nasin Pakalaa!
jan Pepa|9|True|kasi mi li suli, li laso. mun lili en suno lili en kulupu suno lili li lon ona!
jan Pepa|10|False|ona li pona suli lukin!
jan Pepa|11|True|ni li
jan Pepa|12|False|pali pona kin!
jan Pepa|13|True|taso mi ken ala pali e ni lon tomo ni...
jan Pepa|14|False|...mi kepeken wawa pi nasin ante la, jan sona mi li sona. ona li wile e wawa pakala taso.
soweli Kawa|3|False|luka
kalama|2|False|Konnn
jan Pepa|6|False|ala!
jan Pepa|4|False|pakala

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|3|False|Seli
kalama|17|False|SuuNNNOO
sitelen|22|False|lipu KOMONA
jan Pepa|1|True|ken la...
jan Pepa|2|False|sona mi li kama...
jan Pepa|4|False|pona a! lipu pi "wawa pakala" la, jan Tume li toki e ni: ma poki lili li pona tawa ona. taso o lukin e toki ni pi ike ken:
jan Pepa|16|False|mi o pali!
jan Kumin|19|False|seme? jan Pepa li pali ala pali e ma poki?
kalama|18|False|Selllooo! !|nowhitespace
jan Tume|20|True|pona!
sitelen|8|False|“ O TAWA ALA e ijo pali lon ma ante. ike li kama tan ni.
sitelen|6|False|“ O PANA ALA e wawa Lonaa sina ale tawa pali pi ma poki.”
sitelen|11|True|“jan ante li KEN ALA pana e pona tan insa ala pi ma poki...
jan Pepa|7|False|mi sona.
jan Pepa|10|False|ni li pona!
jan Pepa|9|False|a a... mi sona!
jan Pepa|5|False|a...
sitelen|12|False|...jan ala li ken lukin e sina lon insa ! ”
jan Tume|21|False|taso ona o lukin mute e pona e sitelen ale mi lon lipu mi pi wawa pakala.
jan Pepa|15|True|ni li pona a!
jan Pepa|13|True|jan ala
jan Pepa|14|True|anu seme!?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|pona suli!
jan Pepa|2|True|ni li sama poki, li weka tan ale. ona li tu e tomo pi lipu sona. wan li lon. mi kepeken e wan ni taso.
jan Pepa|3|False|poki musi ni li pona mute tawa pali mi.
jan Pepa|4|False|tenpo ni la, mi pali e ijo suli mi!
kalama|5|False|Sunoo
kalama|6|False|Kkama! !|nowhitespace
kalama|8|False|Anntee! !|nowhitespace
kalama|10|False|Panaa! !|nowhitespace
jan Pepa|7|False|a! ala!
jan Pepa|9|False|jaki a!
jan Pepa|11|False|a! ona li pona lukin. taso ona li lili kin...
jan Pepa|13|False|SOWELI KAWA O TAWA ALA!
soweli Kawa|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|False|Suuuliii! !|nowhitespace
jan Pepa|3|True|mi pilin e ni: kasi ni pi kama wan li kasi pona taso. tan ni la, mi wile tawa e ona tan ma poki, tawa tomo mi.
jan Pepa|2|True|mi suli e ona. a! ona li pona mute lukin!
jan Pepa|5|True|pona,
jan Pepa|6|True|tenpo ni la, mi tawa e pali mi tan ma poki. mi pana e pali mi tawa kulupu mi. jan ale o lukin e ni: wawa pi pimeja pakala li ken pali e ijo pona!
kalama|8|False|Pinnii! !|nowhitespace
jan Tume|9|True|o kute e kalama ni! jan Pepa li kama tan ma poki.
jan Tume|10|False|jan Kajen o, sina pona! nasin sina li pali e ni: tenpo ni la, jan Pepa li pali pona.
jan Kajen|11|False|jan lawa Tume o, toki sina li suwi tawa mi.
jan Pepa|7|False|jan sona mi li lukin e pali mi la, oko ona li kama suli mute!
sitelen|12|False|lipu KOMONA
jan Pepa|4|False|kasi pi kama wan li ala pi wawa pakala.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|5|True|LLLlinja
kalama|6|True|LLLINja
kalama|7|True|LLLinja
kalama|8|True|LLinja
kalama|9|True|LLinja
kalama|10|False|Linja
soweli Kawa|11|False|?
soweli Kawa|13|False|!!
kalama|14|True|LLinja
kalama|15|False|LLLinja
soweli Kawa|16|False|!!
jan Pepa|1|True|SOWELI KAWA O!
jan Pepa|2|True|mi tawa. mi kama e jan Tume e jan Kajen e jan Kumin.
jan Pepa|3|False|o pali e ala tawa kasi. sina kute ala kute?
jan Pepa|4|False|pona! mi kama sin.
jan Pepa|17|True|jan sona o, kama. o lukin, o pilin suli! oko sina li kama open suli mute tan pali mi.
jan Pepa|18|False|o kama tawa sewi. sina ken...
kalama|12|False|Suliiii!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen toki|6|False|- PINI -
kalama|1|False|Kamaaa!!
kalama|4|False|PAKALA! !!|nowhitespace
kalama|3|False|Tawaa! !!|nowhitespace
jan Pepa|2|False|?!!
jan Pepa|5|False|ken la, mi kama sona e wawa pi pakala mute a!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|2|False|tenpo 12/2017 - www.peppercarrot.com - musi sitelen & toki: jan David Revoy - ante toki pi toki pona: jan Ret Samys en jan Libre Fan
mama|3|False|jan Craig Maloney en jan CalimeroTeknik en jan Jookia li pona e toki jan.
mama|5|False|musi ni li tan pali pi ma Elewa. pali ni li tan jan David Revoy. jan Craig Maloney li pana e pona tawa ona. jan Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson li pona e musi ni.
mama|6|False|ilo: ilo Krita 3.2.3 en ilo Inkscape 0.92.2 li kepeken ilo Ubuntu 17.10
mama|7|False|nasin pi lawa jo: Creative Commons Attribution 4.0
mama|9|False|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa lon www.patreon.com/davidrevoy
mama|8|False|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 810 li pana e mani la, lipu ni li lon:
mama|4|False|jan Alex Gryson en jan Nicolas Artance en jan Ozdamark en jan Zveryok en jan Valvin en jan xHire li lukin pona, li pona e toki.
mama|1|False|Musi Pona!
