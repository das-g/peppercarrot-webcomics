# Transcript of Pepper&Carrot Episode 25 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 25: Det finst ingen snarvegar

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Skrift|1|False|KATTEMAT

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Forteljar|13|False|– SLUTT –
Pepar|1|True|?
Pepar|2|True|?
Pepar|3|True|?
Pepar|4|False|?
Pepar|5|True|?
Pepar|6|True|?
Pepar|7|True|?
Pepar|8|False|?
Pepar|9|True|?
Pepar|10|True|?
Pepar|11|True|?
Pepar|12|False|?

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|7|False|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot: www.patreon.com/davidrevoy
Bidragsytarar|6|False|Pepar og Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane. Takk til dei 909 som støtta denne episoden:
Bidragsytarar|1|False|Mai 2018 – www.peppercarrot.com – Teikna og fortald av David Revoy – Omsett av Arild Torvund Olsen og Karl Ove Hufthammer
Bidragsytarar|3|False|Bygd på Hereva-universet skapa av David Revoy, med bidrag frå Craig Maloney og rettingar frå Willem Sonke, Moini, Hali, CGand og Alex Gryson.
Bidragsytarar|4|False|Programvare: Krita 4.0.0 og Inkscape 0.92.3 på Kubuntu 17.10
Bidragsytarar|5|False|Lisens: Creative Commons Attribution 4.0
Bidragsytarar|2|False|Tidleg tilbakemelding: Nicolas Artance, Imsesaok, Craig Maloney, Midgard, Valvin, xHire og Zveryok.
