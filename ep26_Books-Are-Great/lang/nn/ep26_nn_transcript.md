# Transcript of Pepper&Carrot Episode 26 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 26: Bøker er fantastiske

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Bøker er fantastiske!
Pepar|3|False|Dette er gull!
Pepar|2|True|I denne sjeldne boka fortel ein røynd eventyrar alt om denne borga.
Pepar|4|False|Til dømes skyt dette vonde auget eldkuler mot ubedne gjestar.
Pepar|5|False|Men dersom du kjem inn frå sida, slik at det ikkje kan sjå deg, ...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|... og legg ein hatt over,
Pepar|3|False|... så opnar døra seg!
Pepar|4|True|Me får endåtil råd om å la hatten henga, for å gjera det lettare å flykta.
Pepar|5|False|Bøker er så fantastiske!
Skrift|6|False|«Hopp rett over det fyrste rekkverket.»
Pepar|7|False|OK!
Lyd|9|False|BOING
Lyd|10|False|BOING
Skrift|8|False|«Du vil få ei kjensle av å falla. Det er heilt normalt. Dette er ein snarveg.»
Skrift|11|False|«Du kan heilt trygt slappa av i den sjølvlysande spidelveven. Desse edderkoppane er fotosyntetiske og livnærer seg av lyset.»
Skrift|12|False|«Eg vil tilrå ein liten lur for å koma til kreftene att.»
Pepar|13|False|Eg elskar bøker!
Lyd|2|False|Poff!
<hidden>|0|False|This “they” means one person, without specifying their gender. If possible, please try to do the same in your language (suggestion: if your language doesn't have genderless pronouns, maybe you can translate as “the author”). If that doesn't really work well, assume the author is a woman.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|2|False|Eg veit at kattar hatar å verta våte.
Pepar|3|True|Slapp av!
Pepar|4|False|Her òg kan ei bok vera til hjelp!
Pepar|5|True|Og mindre enn ti minutt etter at eg gjekk inn porten, sit eg i skattkammeret og plukkar magiske blad frå det sjeldne vasstreet.
Pepar|6|False|Bøker er magiske!
Pepar|1|True|Å, Gulrot ...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Skrift|3|False|«Sidan du tok snarvegen, er du snart i rommet til vaktaren.»
Pepar|4|True|Hm ...
Pepar|5|True|Vaktaren?!
Pepar|6|False|Kvar er han?
Pepar|7|True|AHA!
Pepar|8|False|Fann deg!
Pepar|11|True|Pfff...!
Pepar|12|False|Nesten for lett når du kjenner det veike punktet hans.
Skrift|1|False|«Plukk med deg ei fjør på vegen.»
Pepar|2|False|Ja, her er det nok av fjør!
Lyd|9|True|kitla
Lyd|10|False|kitla

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Sjkrrr!|nowhitespace
Pepar|2|False|Aha! Her er utgangen!
Lyd|3|False|Smokk!
Pepar|5|True|Æææsj ...
Pepar|6|False|Det står heilt sikkert noko om korleis ein kan fjerna denne ... øh ... «greia».
Pepar|7|False|Gulrot ... Kan du lesa for meg?
Pepar|8|True|Nei, sjølvsagt kan du ikkje det.
Pepar|9|False|Pff...
Pepar|4|False|?!
Pepar|12|False|Grrr!!!
Pepar|10|True|EG VISSTE DET!
Pepar|11|True|Det var for godt til å vera sant!
Pepar|13|False|Kan henda det likevel ikkje stemmer at bøker kan løysa alt ...
Lyd|14|False|Paff!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Med mindre ...
Pepar|2|False|J I A A A A A A A ! ! !|nowhitespace
Lyd|3|False|Dunk!|nowhitespace
Pepar|5|False|Bøker er fantastiske!
Pepar|4|True|Jo, det stemmer visst likevel, i alle samanhengar ...
Forteljar|6|False|– SLUTT –

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|Lisens: Creative Commons Attribution 4.0. Programvare: Krita 4.1 og Inkscape 0.92.3 på Kubuntu 17.10. Teikningar og forteljing: David Revoy. Manushjelp: Craig Maloney. Tidleg tilbakemelding: Ambernite, Craig Maloney, CalimeroTeknik, Jookia, Midgard, Nartance, Popolon, Quiralta, Valvin, xHire og Zeograd. Korrekturlesing engelsk versjon: Craig Maloney og CalimeroTeknik Omsetjing til nynorsk: Arild Torvund Olsen og Karl Ove Hufthammer. Bygd på Hereva-universet Skapar: David Revoy. Hovudutviklar: Craig Maloney. Forfattarar: Craig Maloney, Nartance, Scribblemaniac og Valvin. Rettingar: Willem Sonke, Moini, Hali, CGand og Alex Gryson. 28. juli 2018 www.peppercarrot.com
Bidragsytarar|3|False|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot: www.patreon.com/davidrevoy
Bidragsytarar|2|False|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane. Takk til dei 1 098 som støtta denne episoden:
<hidden>|0|False|Remove the names of the people who helped to create the English version and create this section for your language. You can credit translators, proofreaders, whatever categories you want. N.B.: In Inkscape, if you press Enter to create a new line, it will not push down text below it until the line has text. So to create a whiteline, type a space on its own line.
