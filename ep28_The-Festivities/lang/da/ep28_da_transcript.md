# Transcript of Pepper&Carrot Episode 28 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 28: Festlighederne

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fortæller|1|True|De tre Hevera-måner stod på række den nat,
Fortæller|2|False|og deres skær gav et spektakulært syn i Zombiah-katedralen.
Fortæller|3|False|Det var under dette magiske lys, at min veninde Koriander blev kronet til...
Fortæller|4|False|Dronning af Qualicity.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fortæller|1|False|Lidt efter begyndte festlighederne.
Fortæller|2|False|En kæmpe fest med alle trylleskolerne og hundredevis af gæster fra hele Hevera
Pepper|3|False|?!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Åh, det er dig!
Pepper|3|False|Jeg var i mine egne tanker
Carrot|2|False|spinde spinde
Pepper|4|False|Jeg går ind. Det begynder at blive koldt udenfor.
Journalist|6|False|Hurtigt!
Journalist|5|False|Der er hun!
Journalist|7|False|Frøken Safran! Et par ord til Qualicity Dagblad ?
Safran|8|False|Ja, selvfølgelig!
Pepper|15|False|...
Pepper|16|False|Ser du, Carrot ? Jeg tror jeg forstår, hvorfor jeg ikke er i feststemning.
Lyd|9|False|F LASH|nowhitespace
Journalist|13|True|Frøken Safran! Hereva Modeblad.
Lyd|10|False|F LASH|nowhitespace
Lyd|11|False|F LASH|nowhitespace
Lyd|12|False|F LASH|nowhitespace
Journalist|14|False|Hvilken designer har De på i dag?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Alle mine venner er succesfulde, og nogle gange ville jeg gerne have bare en brøkdel af det, de har.
Pepper|2|False|Koriander er dronning.
Pepper|3|False|Safran er rig og berømt.
Pepper|4|False|Selv Shichimi passer perfekt ind i sin skole.
Pepper|5|False|Og hvad med mig? Hvad har jeg?
Pepper|6|False|Det dér.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Pepper?
Shichimi|2|False|Jeg har taget noget mad med. Vil du have noget?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Det føles mærkeligt at gemme sig for at spise.
Shichimi|2|False|Men vores læremester siger, at vi skal fremstå åndelige og uden materielle behov
Safran|3|False|Nå! Der var I!
Safran|4|True|Endelig et sted at gemme sig for fotograferne!
Safran|5|False|De driver mig til vanvid!
Koriander|6|False|Fotografer?
Koriander|7|False|Prøv i stedet at undgå de kedelige samtaler med politikere, når I har den her på hovedet.
Lyd|8|False|kradse' kradse'

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|1|True|Og, Pepper, jeg har endelig mødt dine gudmødre.
Koriander|2|False|De er virkelig noget for sig.
Pepper|3|False|!!!
Koriander|4|True|Jeg mener, du er virkelig heldig.
Koriander|5|False|Jeg er sikker på, de lader dig gøre, hvad du vil.
Koriander|6|False|Som at droppe de officielle positurer og den ubetydelige snak uden at risikere diplomatiske konsekvenser.
Safran|7|False|Eller at danse og have det sjovt og være ligeglad med, hvad de andre tænker.
Shichimi|8|False|Eller at kunne smage alt fra buffeten foran gæsterne.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fortæller|4|False|FORTSÆTTES…
Shichimi|1|False|Åh! Pepper?! Sagde vi noget forkert?
Pepper|2|True|Nej, slet ikke!
Pepper|3|False|Tak, venner!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Du kan også blive tilhænger og få dit navn skrevet her!
Pepper|3|True|Pepper & Carrot er fri, open-source og sponsoreret af sine læsere.
Pepper|4|False|Denne episode blev støttet af 960 tilhængere!
Pepper|7|True|Gå på www.peppercarrot.com og få mere information
Pepper|6|True|Vi er på Patreon, Tipeee, PayPal, Liberapay ... og andre!
Pepper|8|False|Tak!
Pepper|2|True|Vidste du det?
Credits|1|False|Januar 2019 Tegning og manuskript: David Revoy Genlæsning i beta-versionen: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin. Dansk Version Oversættelse: Emmiline Alapetite Rettelser: Rikke & Alexandre Alapetite Baseret på Hereva-universet Skabt af: David Revoy Vedligeholdt af: Craig Maloney. Medforfattere: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Rettelser: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Værktøj: Krita 4.1.5~appimage, Inkscape 0.92.3 on Kubuntu 18.04.1. Licens: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
