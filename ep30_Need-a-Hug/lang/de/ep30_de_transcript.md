# Transcript of Pepper&Carrot Episode 30 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 30: Zuneigung gesucht

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Erzähler|1|False|- ENDE -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Du kannst auch Gönner von Pepper&Carrot werden und deinen Namen hier lesen!
Pepper|3|True|Pepper&Carrot ist vollständig frei(libre), Open Source und finanziert durch Spenden von Lesern.
Pepper|4|False|Für diese Episode danken wir 973 Gönnern!
Pepper|7|True|Geh auf www.peppercarrot.com für mehr Informationen!
Pepper|6|True|Wir sind auf Patreon, Tipeee, PayPal, Liberapay ...und mehr!
Pepper|8|False|Dankeschön!
Pepper|2|True|Wusstest du schon?
Impressum|1|False|03. September, 2019 Illustration & Handlung : David Revoy. Beta-Leser des Storyboards: Alina the Hedgehog, Craig Maloney, Jihoon Kim, Parnikkapore, Martin Disch, Nicolas Artance, Valvin. Deutsche Version Übersetzung : Alina The Hedgehog, Martin Disch. Basierend auf der Hereva-Welt Erstellung : David Revoy. Hauptbetreuer : Craig Maloney. Autoren : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Korrektur: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software : Krita/4.2~git branch, Inkscape 0.92.3 auf Kubuntu 18.04.2. Lizenz : Creative Commons Namensnennung 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
