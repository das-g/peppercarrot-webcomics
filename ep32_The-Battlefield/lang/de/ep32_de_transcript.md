# Transcript of Pepper&Carrot Episode 32 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 32: Das Schlachtfeld

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
König|1|True|Hauptmann!
König|2|False|Habt Ihr wie befohlen eine Hexe angeheuert?
Hauptmann|3|True|Jawohl, mein Herr!
Hauptmann|4|False|Sie steht neben Euch.
König|5|False|...?
Pepper|6|True|Hallöchen!
Pepper|7|False|Ich bin Pepp...
König|8|False|?!!
König|9|True|IDIOT!!!
König|10|True|Weshalb habt Ihr mir dieses Kind gebracht?!
König|11|False|Ich brauche eine echte Kriegshexe!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Entschuldigen Sie mal!
Pepper|2|True|Ich bin eine echte Chaosah-Hexe.
Pepper|3|False|Ich habe sogar ein Diplom, das...
Schrift|4|True|Abschluss
Schrift|5|True|von
Schrift|6|False|Chaosāh
Schrift|7|False|Cayenne
Schrift|8|False|Kümmel
Schrift|9|False|Thymian
Schrift|10|False|~ für Pepper ~
König|11|False|SCHWEIG!
König|12|True|Ich habe keine Verwendung für Kinder in dieser Armee.
König|13|False|Geh nach Hause und spiel mit deinen Puppen.
Geräusch|14|False|Klatsch!
Armee|15|True|HAHA HA HA!
Armee|16|True|HAHA HA HA!
Armee|17|True|HAHA HA HA!
Armee|18|False|HA HA HAHA!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Unfassbar!
Pepper|2|False|Ich habe jahrelang gelernt, aber niemand nimmt mich ernst, weil...
Pepper|3|False|...ich nicht erfahren genug aussehe!
Geräusch|4|False|PUFF ! !
Pepper|5|False|CARROT!
Geräusch|6|False|PAFF ! !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Echt jetzt, Carrot?
Pepper|2|True|Ich hoffe das Essen war's wert.
Pepper|3|False|Du siehst fürchterlich aus...
Pepper|4|False|...Du siehst...
Pepper|5|True|Das Aussehen!
Pepper|6|False|Natürlich!
Geräusch|7|False|Knack !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|HEY!
Pepper|2|False|Ich hörte, ihr sucht eine ECHTE HEXE?!?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
König|1|True|Nach reichlicher Überlegung, ruft dieses Kind zurück.
König|2|False|Die hier ist wahrscheinlich zu teuer.
Schrift|3|False|FORTSETZUNG FOLGT…

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Du kannst auch Gönner von Pepper&Carrot werden und deinen Namen hier lesen!
Pepper|3|True|Pepper&Carrot ist vollständig frei(libre), Open Source und finanziert durch Spenden von Lesern.
Pepper|4|False|Für diese Episode danken wir 1121 Gönnern!
Pepper|7|True|Geh auf www.peppercarrot.com für mehr Informationen!
Pepper|6|True|Wir sind auf Patreon, Tipeee, PayPal, Liberapay ...und mehr!
Pepper|8|False|Dankeschön!
Pepper|2|True|Wusstest du schon?
Impressum|1|False|31. März 2020 Illustration & Handlung: David Revoy. Beta-Leser: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Parnikkapore, Stephen Paul Weber, Valvin, Vejvej. Deutsche Version Übersetzung: Martin Disch. Korrektur: Alina The Hedgehog, Ret Samys . Basierend auf der Hereva-Welt Erstellung: David Revoy. Hauptbetreuer: Craig Maloney. Autoren: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korrektur: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.2.9-beta, Inkscape 0.92.3 auf Kubuntu 19.10. Lizenz: Creative Commons Namensnennung 4.0. www.peppercarrot.com
<hidden>|9|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|10|False|You can also translate this page if you want.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
