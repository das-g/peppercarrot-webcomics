# Transcript of Pepper&Carrot Episode 34 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 34: Shichimi slås til ridder

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fortæller|1|False|Den samme aften...
Hibiscus|2|False|...så i aften siger vi velkommen til dig, Shichimi, som vores yngste Ah-ridder.
Koriander|3|False|Stadig intet spor af Pepper?
Safran|4|False|Ikke endnu.
Safran|5|False|Hun burde skynde sig, ellers går hun glip af Shichimis tale.
Shichimi|6|False|Tak.
Shichimi|7|False|Jeg vil gerne...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Ziu uUUUU|nowhitespace
Lyd|2|False|Ziuu U U U UU|nowhitespace
Pepper|3|True|Ankommer!
Pepper|4|True|Pas på!
Pepper|5|False|PAS PÅ!!!
Lyd|6|False|KR A S H!|nowhitespace
Lyd|7|False|Ups!
Lyd|8|False|Er alle okay? Intet brækket?
Shichimi|9|False|Pepper!
Pepper|10|True|Hej Shichimi!
Pepper|11|True|Undskyld den dramatiske ankomst og forsinkelsen!
Pepper|12|False|Jeg har løbet hele dagen, men det er en lang historie.
<hidden>|13|False|Dear translator: if editing the soundFX is difficult, I made a special documentation here: https://www.peppercarrot.com/en/static14/documentation&page=055_Sound-Effect_translation

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hibiscus|1|False|Er hun en af dine gæster?
Shichimi|2|False|Ja. Det er min veninde Pepper. Alt er fint.
Pepper|3|True|Carrot, er du okay?
Pepper|4|False|Undskyld landingen, jeg mestrer ikke hyper-hastigheden endnu.
Pepper|5|True|Og igen undskyld for besværet
Pepper|6|False|og for hvordan jeg er klædt...
Shichimi|7|False|ti hi
Wasabi|8|True|Shichimi,
Wasabi|9|False|den unge heks som lige er ankommet, er hun virkelig én af dine venner?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Ja, Deres Højhed.
Shichimi|2|False|Hendes navn er Pepper af Kaosah-skolen.
Wasabi|3|True|Hendes tilstedeværelse forurener vores skoles hellige natur.
Wasabi|4|False|Få hende ud af mit øjesyn med det samme.
Shichimi|5|True|Men...
Shichimi|6|False|Mester Wasabi...
Wasabi|7|True|Men hvad ?
Wasabi|8|False|Vil du hellere blive forvist fra skolen?
Shichimi|9|False|! ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Undskyld Pepper, men du er nødt til at gå.
Shichimi|2|False|Nu.
Pepper|3|True|Hvad?
Pepper|4|False|Hey hey hey, vent lige! Jeg er sikker på, at det er en misforståelse.
Shichimi|5|False|Vær nu sød Pepper, gør det ikke sværere.
Pepper|6|False|Hey! Dig dér, på tronen. Hvis du har et problem med mig, så kom selv ned og sig det!
Wasabi|7|False|Tsss...
Wasabi|8|True|Shichimi, du har ti sekunder...
Wasabi|9|True|ni...
Wasabi|10|True|otte...
Wasabi|11|False|syv...
Shichimi|12|False|SÅ ER DET NOK, PEPPER! GÅ VÆK!!!
Lyd|13|False|SHRRIiii!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Shichimi, vær nu sød, slap...
Lyd|2|False|B ADuuM!|nowhitespace
Shichimi|3|True|GÅ VÆK!!!
Shichimi|4|True|GÅ VÆK!!!
Shichimi|5|False|GÅ VÆK!!!
Lyd|6|False|K R E E E E E! !!|nowhitespace
Pepper|7|True|Av!
Pepper|8|False|Hey! Det er... i-ikke... Av... sødt!
Koriander|9|False|SHICHIMI! PEPPER! VÆR NU SØDE AT STOPPE!
Safran|10|False|Vent.
Wasabi|11|False|Hmm!
Pepper|12|True|Grrr!
Pepper|13|False|OK, du har selv bedt om det!
Lyd|14|False|B R ZUU !!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|CURSUS CANCELLARE MAXIMUS!!!
Lyd|2|False|S H K L AK!|nowhitespace
Pepper|3|False|Av!
Lyd|4|False|P AF !!|nowhitespace
Shichimi|5|True|Selv din bedste annullerings-formular har ingen effekt på mig!
Shichimi|6|True|Giv op, Pepper, og gå væk!
Shichimi|7|False|Stop med at få mig til at såre dig!
Pepper|8|False|Åh, min annullerings-formular virkede fint, men du var ikke målet.
Shichimi|9|True|Hvad?
Shichimi|10|False|Hvad mener du?!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|?!!
Pepper|2|True|Hun var målet!
Pepper|3|False|Jeg annullerede hendes Glamour-formular som får hende til at se ung ud.
Pepper|4|True|Jeg lagde mærke til det, så snart jeg kom.
Pepper|5|False|Så jeg gav dig en lille smagsprøve på, hvad du fortjener for at få Shichimi til at kæmpe mod mig!
Wasabi|6|True|UFORSKAMMET!
Wasabi|7|True|Hvor vover du,
Wasabi|8|False|og foran hele min skole!
Pepper|9|True|Betragt dig selv som heldig!
Pepper|10|False|Hvis jeg havde hele min Rea,så ville jeg ikke have stoppet dér.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|DR Z OW!!|nowhitespace
Wasabi|2|True|Aha. Du har nået dine forgængeres niveau meget hurtigere end jeg havde forventet...
Wasabi|3|False|Det sætter skub i mine planer, men det er gode nyheder.
Pepper|4|True|Dine planer?
Pepper|5|True|Så du ville bare teste mig? Det havde intet med min forsinkelse af gøre?
Pepper|6|False|Du er virkelig twisted!
Wasabi|7|True|ti...
Wasabi|8|False|hi.
Wasabi|9|True|HVAD STIRRER I ALLE SAMMEN PÅ?!
Wasabi|10|False|JEG ER LIGE BLEVET ANGREBET, OG I STÅR BARE DÉR?! FANG HENDE!!!
Wasabi|11|False|JEG VIL HAVE HENDE I LIVE!
Wasabi|12|False|FANG HENDE!!!
Pepper|13|False|Shichimi, vi må tale om det her senere!
Pepper|14|True|Beklager Carrot, men vi må tage afsted i hyper-hastighed igen.
Pepper|15|False|Hold godt fast!
Lyd|16|False|Tap!
Lyd|17|False|Tap!
Lyd|18|False|Ziiuu UU!!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|? !
Wasabi|2|False|FANG HENDE!!!
Pepper|3|False|Åh gud.
Safran|4|False|Pepper, tag min kost!
Lyd|5|False|Fisss!
Pepper|6|True|Åh wow!
Pepper|7|False|Mange tak, Safran!
Lyd|8|False|Tok!
Lyd|9|False|ziuu U U U U U!|nowhitespace
Fortæller|10|False|FORTSÆTTES...

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Den 31. marts, 2021 Tegning og manuskript: David Revoy. Genlæsning i beta-versionen: Arlo James Barnes, Carotte, Craig Maloney, Efrat b, GunChleoc, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Dansk Version Oversættelse: Emmiline Alapetite Rettelser: Rikke & Alexandre Alapetite Særlig tak til: Nartance for at have udforsket Wasabi-karakteren i hans fan-fiction. Måden han forestillede sig hende på, havde en stor indflydelse på, hvordan jeg afbildede hende i denne episode. Baseret på Hereva-universet Skabt af: David Revoy. Vedligeholdt af: Craig Maloney. Medforfattere: Craig Maloney, Nartance, Scribblemaniac, Valvin. Rettelser: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Værktøj: Krita 4.4.1, Inkscape 1.0.2 på Kubuntu Linux 20.04. Licens: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|True|Vidste I det?
Pepper|3|True|Pepper & Carrot er fri, open-source og sponsoreret af sine læsere.
Pepper|4|False|Denne episode blev støttet af 1096 tilhængere!
Pepper|5|True|Du kan også blive tilhænger og få dit navn skrevet her!
Pepper|6|True|Vi er på Patreon, Tipeee, PayPal, Liberapay … og andre!
Pepper|7|False|Gå på www.peppercarrot.com og få mere information!
Pepper|8|False|Tak!
<hidden>|9|False|You can also translate this page if you want.
<hidden>|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
