# Transcript of Pepper&Carrot Episode 34 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 34: Shichimi's inwijding

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Verteller|1|False|Later die nacht...
Hibiscus|2|False|...en dus, op deze avond, verwelkomen we jou, Shichimi, hierbij als kers-verse Ridder van Ah.
Koriander|3|False|Nog altijd geen teken van Pepper?
Saffraan|4|False|Nog steeds niet.
Saffraan|5|False|Ze kan zich beter haasten, anders mist ze Shichimi's toespraak nog.
Shichimi|6|False|Dank.
Shichimi|7|False|Ik wil graag...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|False|Zjo oEEEEE|nowhitespace
Geluid|2|False|Zjoo OE EEE|nowhitespace
Pepper|3|True|Van onderen!
Pepper|4|True|KIJK UIT!
Pepper|5|False|KIJK UIT!!!
Geluid|6|False|BAAAAM!|nowhitespace
Geluid|7|False|Oepsie!
Geluid|8|False|Is iedereen in orde? Niets gebroken?
Shichimi|9|False|Pepper!
Pepper|10|True|Hooooi, Shichimi!
Pepper|11|True|Sorry dat ik zo binnen kom valllen, en dat ik te laat ben!
Pepper|12|False|Ik ben de hele dag van hot naar her gevlogen en... lang verhaal.
<hidden>|13|False|Dear translator: if editing the soundFX is difficult, I made a special documentation here: https://www.peppercarrot.com/en/static14/documentation&page=055_Sound-Effect_translation

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hibiscus|1|False|Is dit een van de genodigden?
Shichimi|2|False|Ja. Mijn goede vriendin Pepper. Alles is in orde.
Pepper|3|True|Carrot, ben je oké?
Pepper|4|False|Het spijt me, ik heb het landen op supersnellheid nog niet zo goed onder de knie, geloof ik.
Pepper|5|True|Nogmaals mijn excuses voor de onderbreking,
Pepper|6|False|ennn... ook voor m'n outfit....
Shichimi|7|False|Hihi!
Wasabi|8|True|Shichimi,
Wasabi|9|False|deze jonge heks die zojuist gearriveerd is, is zij waarlijk een van jouw vriendinnen?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Jazeker, Uwe Hoogheid.
Shichimi|2|False|Haar naam is Pepper, van de Chaosah-school.
Wasabi|3|True|Haar aanwezigheid vervuilt de gezegende aard van onze School.
Wasabi|4|False|Verwijder haar uit mijn zicht, en wel meteen.
Shichimi|5|True|Maar...
Shichimi|6|False|Meester Wasabi...
Wasabi|7|True|Wat "maar "?
Wasabi|8|False|Heb je liever dat jullie beiden verbannen zijn van deze school?
Shichimi|9|False|! ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Het spijt me, Pepper, maar je moet gaan.
Shichimi|2|False|Nu.
Pepper|3|True|Hè?
Pepper|4|False|Ho, ho, wacht eens even! Er is vast sprake van een misverstand.
Shichimi|5|False|Alsjeblieft, Pepper, maak het niet zwaarder dan het al is.
Pepper|6|False|Hé! Jij daar op die troon! Als je een probleem hebt met mij, kom het dan zelf tegen me zeggen!
Wasabi|7|False|Pfff...
Wasabi|8|True|Shichimi, je krijgt nog tien seconden de tijd...
Wasabi|9|True|negen...
Wasabi|10|True|acht...
Wasabi|11|False|zeven...
Shichimi|12|False|GENOEG, PEPPER! GA WEG!!!
Geluid|13|False|SHRRIiii!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Shichimi, alsjeblieft, kalm aa...
Geluid|2|False|B ADoeM !|nowhitespace
Shichimi|3|True|GA WEG!!!
Shichimi|4|True|GA WEG!!!
Shichimi|5|False|GA WEG!!!
Geluid|6|False|C R E E E E E! !!|nowhitespace
Pepper|7|True|Auw!
Pepper|8|False|Hé! Dit is... n-NIET... oeeh... aardig!
Koriander|9|False|SHICHIMI! PEPPER! STOP HIERMEE, ALSJEBLIEFT!
Saffraan|10|False|Wacht.
Wasabi|11|False|Hmm!
Pepper|12|True|Grrr!
Pepper|13|False|OKÉ, je vraagt erom!
Geluid|14|False|BZ J OE!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|CURSUS CANCELLARE MAXIMUS!!!
Geluid|2|False|S H K L AK!|nowhitespace
Pepper|3|False|Auw!
Geluid|4|False|P AF !!|nowhitespace
Shichimi|5|True|Zelfs jouw sterkste onttoverspreuk heeft op mij geen enkel effect!
Shichimi|6|True|Geef toch op, Pepper, en vertrek!
Shichimi|7|False|Dwing me niet om je nog meer pijn te doen!
Pepper|8|False|Oh, mijn onttoverspreuk heeft wel degelijk effect gehad, jij was simpelweg niet het doel.
Shichimi|9|True|Heh?
Shichimi|10|False|Hoe bedoel je?!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|?!!
Pepper|2|True|Zij was mijn doelwit!
Pepper|3|False|Ik heb gewoonweg haar Verjongingsbetovering, die haar jonger deed lijken, opgeheven.
Pepper|4|True|Het viel me al op meteen nadat ik hier aankwam.
Pepper|5|False|En dit is nog het minste dat je verdient voor het feit dat je Shichimi gedwongen hebt mij aan te vallen!
Wasabi|6|True|BRUTAAL NEST!
Wasabi|7|True|Hoe durf je,
Wasabi|8|False|en dat in het bijzijn van mijn gehele School!
Pepper|9|True|Wees blij!
Pepper|10|False|Als ik al mijn Rea had gehad, dan had ik het hier niet bij gelaten!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|False|DZJ OE!!|nowhitespace
Wasabi|2|True|Goed dan. Ik zie dat je het niveau van je voorgangers sneller geëvenaard hebt dan ik verwachtte...
Wasabi|3|False|Dat dwingt me mijn plannen te versnellen, maar dat is alleen maar goed nieuws.
Pepper|4|True|Je plannen?
Pepper|5|True|Dit alles was alleen maar een test, iets wat niks te maken had met mijn te laat komen?
Pepper|6|False|Je bent bespottelijk!
Wasabi|7|True|Ha...
Wasabi|8|False|ha.
Wasabi|9|True|WAT STAAN JULLIE DAAR MAAR WAT TE KIJKEN?
Wasabi|10|False|IK WORDT AANGEVALLEN EN JULLIE STAAN DAAR MAAR?! GRIJP HAAR!!!
Wasabi|11|False|Ik wil haar levend hebben!
Wasabi|12|False|NEEM HAAR GEVANGEN!!!
Pepper|13|False|Shichimi, we spreken hier later nog wel over!
Pepper|14|True|Het spijt me, Carrot, maar we zullen nog een keer op supersnelheid moeten vertrekken.
Pepper|15|False|Zet je schrap!
Geluid|16|False|Tik!
Geluid|17|False|Tik!
Geluid|18|False|ZjooEE !!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|? !
Wasabi|2|False|GRIJP HAAR!!!
Pepper|3|False|Verdorie.
Saffraan|4|False|Pepper, neem mijn bezem!
Geluid|5|False|Fizzz !
Pepper|6|True|Oh, super!
Pepper|7|False|Dankjewel, Safran!
Geluid|8|False|Tok!
Geluid|9|False|zjoo O E E E!|nowhitespace
Verteller|10|False|WORDT VERVOLGD...

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|1|False|Pepper&Carrot is helemaal vrij,open-bron en gesponsord door de giften van haar lezers.
Pepper|2|True|31 maart 2021 Tekeningen & verhaal: David Revoy. Bèta-feedback: Arlo James Barnes, Carotte, Craig Maloney, Efrat b, GunChleoc, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Nederlandse versie Vertaling: Julien Bertholon, Marno van der Maas. Met bijzondere dank aan: Nicolas Artance voor het ontwikkelen en verdiepen van het personage Wasabi in zijn fan-fictiewerk. De manier waarop hij haar weergegeven heeft, heeft een grote invloed gehad op manier waarop ik haar in deze aflevering afgebeeld heb. Gebaseerd op het Hereva-universum Bedenker: David Revoy. Hoofdonderhouder: Craig Maloney. Schrijvers: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Verbeteraars: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.4.1, Inkscape 1.0.2 op Kubuntu Linux 20.04. Licentie: Creative Commons Naamsvermelding 4.0. www.peppercarrot.com
Pepper|3|True|Wist je dat?
Pepper|4|False|Voor deze aflevering bedank ik 1096 patronen!
Pepper|5|True|Jij kan ook patroon van Pepper&Carrot worden en dan komt jouw naam hierbij!
Pepper|6|True|Je kan doneren met Patreon, Tipeee, PayPal, Liberapay ... en meer!
Pepper|7|False|Kijk op www.peppercarrot.com voor alle info!
Pepper|8|False|Dank je!
<hidden>|9|False|You can also translate this page if you want.
<hidden>|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
