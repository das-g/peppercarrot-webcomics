# Transcript of Pepper&Carrot Episode 35 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Episode 35: Refleksjonen

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Å, du er vaken!
Pepar|2|False|Alt i orden? Du frys ikkje?
Pepar|3|True|Eg er dessverre snart tom for rea,
Pepar|4|False|og kraftfeltet mitt er ikkje tjukt nok til å verna oss mot kulda her oppe.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|Snart mistar eg hyperfarten,
Pepar|2|True|og draken og drake-ryttaren har halde tritt med oss i timevis.
Pepar|3|False|Når me sakkar farten, tek dei oss snart igjen.
Pepar|4|True|For ein tosk eg var!
Pepar|5|False|Eg var viss på at dei skulle gje opp for mange timar sidan!
Pepar|6|False|Om me berre hadde klart å rista dei av oss.
Pepar|7|True|Nei, me må kvitta oss heilt med dei!
Pepar|8|False|Og det før dei når oss att!
Pepar|9|True|Men så er eg så ...
Pepar|10|True|... utruleg ...
Pepar|11|False|... trøytt ...
Pepar|12|True|Oi sann!
Pepar|13|False|Full skjerpings, Pepar!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Såg du det?
Arra|2|True|Ja, no er det ikkje lenge att.
Arra|3|False|Snart er ho heilt tom for kraft.
Torreya|4|False|Og då går me til åtak!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|BR A K !|nowhitespace
Pepar|2|False|KVA?!
Pepar|3|False|Braut dei lydmuren?!
Pepar|4|False|! ! !|nowhitespace
Lyd|5|False|VOoos J ! !!|nowhitespace
Pepar|6|True|Grrr!
Pepar|7|False|Hald deg fast, Gulrot!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|SØREN!
Pepar|2|False|KVA GJER ME NO?!
Pepar|3|False|Å!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Hornsjøgrotta!
Pepar|2|False|Her har me ein sjanse, Gulrot!
Pepar|3|False|Eg kjenner denne grotta.
Pepar|4|True|Ho har ein lang, buktande hòlegang som endar i eit samanrasa rom.
Pepar|5|True|Dryppsteinane frå taket vart liggjande som ein vegg med piggar. Det er ei dødsfelle!
Pepar|6|True|Det finst ei lita opning som er nett stor nok til at me kjem oss ut.
Pepar|7|False|Men draken har ikkje sjanse til å styra unna, så dei kjem til å treffa veggen med full kraft! Muhahaha!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Perfekt! Dei følgjer etter oss!
Pepar|2|False|No er me snart kvitt dei.
Pepar|3|True|Og det vert litt av eit syn!
Pepar|4|False|Muhahaha!
Pepar|5|False|! ! !|nowhitespace

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|Vent! Kva har eg blitt?
Pepar|2|True|Ein mordar?
Pepar|3|False|Ei vond heks?
Pepar|4|False|! ! !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|4|False|Svoosj!
Pepar|1|True|NEI!
Pepar|2|False|Det er ikkje slik eg er!
Pepar|3|False|STOOOPP!!!
Pepar|5|False|EG OVERGJEV MEG!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Du veit at det umogleg kan enda godt for deg om du overgjev deg til meister Wasabi?
Pepar|2|False|Eg er klar over det. Men eg trur likevel det finst ein måte å få henne til å forstå.
Pepar|3|True|Alt kan gjerast godt att.
Pepar|4|False|Det lærte eg av ei god veninne.

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Takk for at du lét Gulrot henta reine klede til meg.
Torreya|2|True|Berre hyggjeleg.
Torreya|3|False|Du gjorde arbeidet vårt lettare, så det minste eg kunne gjera, var å gjengjelda tenesta.
Torreya|4|True|Men du veit at Wasabi er skikkeleg sur på deg?
Torreya|5|True|Ho krev at alt er perfekt. Du øydela ved å vera upassande kledd, vera skiten og ved å forstyrra seremonien.
Torreya|6|False|Og du gjekk til åtak på henne! Ho kjem ikkje til å vera nådig.
Pepar|7|False|Eg er klar over det.
Torreya|8|True|Men no treng eg ein liten blund.
Torreya|9|True|Me dreg når eg vaknar.
Torreya|10|False|Gjer som du vil i mellomtida, men hald deg i nærleiken ...
Lyd|11|False|Splasj
Torreya|12|False|For ei merkeleg heks.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|No, same kva farar og kniper eg hamnar i,
Pepar|2|True|vil eg vera tru mot den eg er.
Pepar|3|False|Det lovar eg!
Forteljar|4|False|FRAMHALD FØLGJER ...

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|18. juni 2021 Teikningar og forteljing: David Revoy. Tidleg tilbakemelding: Arlo James Barnes, Craig Maloney, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore og Valvin. Omsetjing til nynorsk Karl Ove Hufthammer og Arild Torvund Olsen. Bygd på Hereva-universet Skapar: David Revoy. Hovudutviklar: Craig Maloney. Forfattarar: Craig Maloney, Nartance, Scribblemaniac og Valvin. Rettingar: Willem Sonke, Moini, Hali, CGand og Alex Gryson . Programvare: Krita 4.4.2 og Inkscape 1.1 på Kubuntu Linux 20.04. Lisens: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepar|3|True|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane.
Pepar|4|False|Takk til dei 1 054 som støtta denne episoden!
Pepar|2|True|Visste du dette?
Pepar|5|True|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot og få namnet ditt her!
Pepar|7|True|Sjå www.peppercarrot.com for meir informasjon!
Pepar|6|True|Me er på blant anna Patreon, Tipeee, PayPal og Liberapay ...
Pepar|8|False|Tusen takk!
<hidden>|9|True|You can also translate this page if you want.
<hidden>|10|True|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
