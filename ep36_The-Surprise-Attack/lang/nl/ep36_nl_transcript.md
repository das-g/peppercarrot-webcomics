# Transcript of Pepper&Carrot Episode 36 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 36: De verrassingsaanval

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Wasabi|1|True|EXCUSES?!
Wasabi|2|False|Wat denk je wel niet?!
Wasabi|3|False|GOOI HAAR DIRECT IN DE GEVANGENIS!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|False|Psjjj ...
Pepper|2|True|Verdorie!
Pepper|3|False|Ik ben hier machteloos!
Pepper|4|False|Verdraaide magische cel! Grrrr!
Geluid|5|False|KLING !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Hoe kon ik toch zo naïef zijn!
Shichimi|2|True|Shhhhh Pepper!
Shichimi|3|False|Maak niet zoveel lawaai.
Pepper|4|False|Wie is daar?!
Shichimi|5|True|Shhhhh! Wees stil!
Shichimi|6|True|Kom hier.
Shichimi|7|False|Ik kom je bevrijden.
Geluid|8|False|Dzjii...
Pepper|9|False|Shichimi?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Sorr... het spijt me voor wat er gebeurd is...
Shichimi|2|True|...je weet wel, voor ons gevecht.
Shichimi|3|True|Ik...
Shichimi|4|False|Ik kon niet anders.
Pepper|5|True|Maak je geen zorgen. Ik snap het.
Pepper|6|False|Bedankt dat je gekomen bent.
Shichimi|7|False|Deze magische cel is écht krachtig. Ze hebben alles uit de kast getrokken!
Pepper|8|False|Haha!
Shichimi|9|False|Niet zo hard! Dadelijk horen ze ons nog!
Rat|10|True|LAP
Rat|11|True|LAP
Rat|12|False|LAP
Shichimi|13|True|Luister,
Shichimi|14|True|ik ben hier ook, omdat ik na de ceremonie ben toegetreden tot Wasabi's vertrouwenscirkel...
Shichimi|15|False|Én, zo achter haar plannen ben gekomen...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|6|True|KLING !
Shichimi|7|True|KLING !
Shichimi|8|False|KLANG !
Pepper|9|True|KLONG !
Pepper|10|True|KLING !
Pepper|11|False|Het is afschuwelijk, Pepper.
Shichimi|12|True|Wasabi wil gewoon alle andere Scholen van Magie volledig aan haar onderwerpen ...
Shichimi|13|True|Ze vertrekt morgen bij zonsopgang met een leger heksen naar Kwalistad...
Shichimi|14|False|Oh nee!
Shichimi|15|False|Koriander.
Geluid|16|False|En haar koninkrijk!
Pepper|17|True|En de magie van Zombiah!
Pepper|18|False|We moeten haar zo snel mogelijk waarschuwen.
Rat|19|False|Een drakenrijder en haar draak wachten ons op het dak op om ons hier vandaan te brengen.
Carrot|20|False|Gelukt! Het slot is eindelijk open!
Rat|21|False|Dzjing!
Geluid|1|True|Bravo!
Geluid|2|True|Ik ga snel Carrot oppakken. En mijn hoed.
Geluid|3|True|pieeep
Geluid|4|True|chchchchch!
Geluid|5|False|pieeeeep!
Pepper|22|False|! ! !
Shichimi|23|False|! ! !
Wachter|24|True|PORTIER!
Wachter|25|True|ALARM!
Wachter|26|False|EEN INDRINGER WIL DE GEVANGENE BEVRIJDEN!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Zo dichtbij... we waren bijna ontsnapt.
Pepper|2|False|Ja, zo dichtbij...
Pepper|3|False|Weet je trouwens waarom Wasabi het zo op mij voorzien heeft?
Shichimi|4|True|Ze is bang voor de heksen van Chaosah, Pepper...
Shichimi|5|True|En doodsbang voor die kettingreacties
Shichimi|6|False|van jullie, dat die een grote bedreiging voor haar plannen vormen.
Pepper|7|True|Ah, die, pfff...
Pepper|8|False|Ze hoeft zich heus geen zorgen te maken, hoor! Ik heb er nog nooit zo een in gang weten te zetten.
Shichimi|9|False|Echt niet?
Pepper|10|False|Nee, echt, hahaha!
Shichimi|11|True|Hihihi!
Shichimi|12|False|Zij is zó paranoïde...

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koning|1|True|Officier!
Koning|2|False|Kun je bevestigen dat dit inderdaad de tempel van die heks is?
Officier|3|True|Het is zeer waarschijnlijk, Majesteit.
Officier|4|False|Meerdere van onze informanten hebben haar hier recentelijk waargenomen.
Koning|5|True|GRrrr...
Koning|6|False|Hier woont dus die heks die een dreiging vormt voor onze tradities en onze krijgskunde !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koning|1|False|Laten we onze alliantie vieren door wraak te nemen via het wegvagen van dit kasteel van de bodem van Hereva.
Geluid|2|False|Klap
Vijand|3|False|M ooi gezegd!
Leger|4|True|Ouais !
Leger|5|True|Yâaa !
Leger|6|True|Yaaa !
Leger|7|True|Ouaiiiis !
Leger|8|True|Yihaaa !
Leger|9|True|Yuurr !
Leger|10|True|Yihaaa !
Leger|11|True|Yihaaa !
Leger|12|True|Ouais !
Leger|13|False|Houra !
Koning|14|True|KATAPULTEN!
Koning|15|False|VUUR!!!
Geluid|16|False|hahWOEEEEEEEEEE !
Geluid|17|True|Whooop!
Geluid|18|False|Whooop !
Koning|19|False|AAANVALLUUUH!!!
Pepper|20|True|Wat gebeurt er?
Pepper|21|False|Worden we aangevallen?!
Geluid|22|True|B OE M!
Geluid|23|False|B OEE M!
Shichimi|24|False|Wat?!
Geluid|25|True|KA~B OE M!
Geluid|26|False|BAM M!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|KUCH
Pepper|2|False|KUCH! !
Pepper|3|False|Shichimi! Alles in orde?
Shichimi|4|True|Ja, ik heb niets gebroken!
Shichimi|5|True|En jij?
Shichimi|6|False|En Carrot?
Pepper|7|False|Wij zijn in orde.
Pepper|8|True|Krijg nou wat...
Pepper|9|False|Dit is on... ge... lofelijk...
Shichimi|10|True|Waar komen die legers ineens vandaan?!
Shichimi|11|True|Katapulten?!
Shichimi|12|False|Wat komen ze hier doen?!
Pepper|13|True|Geen idee...
Pepper|14|False|Maar die twee daar ken ik.
Shichimi|15|False|?
Shichimi|16|False|! ! !
Shichimi|17|False|Torreya, hiero!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|True|Shichimi!
Torreya|2|False|Geprezen zijn de Geesten! Je bent veilig en wel.
Geluid|3|False|Klap
Torreya|4|True|Ik was zo ongerust toen ik hoorde dat je gevangen genomen was!
Torreya|5|True|En dit gevecht!
Torreya|6|False|Wat een chaos!
Shichimi|7|False|Torreya, het is zo fijn je weer te zien.
Pepper|8|True|Ooooh...
Pepper|9|True|De drakenrijdster en Shichimi hebben verkering met elkaar...
Pepper|10|True|Onvoorstelbaar wat er gebeurd was geweest als ik haar op die spiezen in had laten vliegen.
Pepper|11|True|En die legers moeten mij wel gevolgd hebben, en zonder hen waren we niet bevrijd geweest.
Pepper|12|True|Dit alles lijkt zó met elkaar verbonden ...
Pepper|13|False|...OOH!

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Wat is er met haar?
Shichimi|2|False|Pepper? Gaat het een beetje?
Pepper|3|True|Ja, prima.
Pepper|4|False|Ik heb me net iets gerealiseerd, dat is alles.
Pepper|5|True|Al deze gebeurtenissen, de directe of indirecte gevolgen van mijn acties en keuzes...
Pepper|6|False|...zijn onbedoeld mijn kettingreactie!
Shichimi|7|True|Echt waar?
Shichimi|8|False|Dat moet je me even uitleggen.
Torreya|9|True|Genoeg gekletst, we staan midden in het slagveld.
Torreya|10|True|We hebben in de lucht alle tijd om bij te praten.
Torreya|11|False|Spring achterop!
Shichimi|12|True|Torreya heeft gelijk.
Shichimi|13|False|We moeten hoe dan ook in Kwalistad zien te komen.
Pepper|14|False|Nee, wacht even.
Pepper|15|True|Het leger van Wasabi maakt zich klaar voor de tegenaanval.
Pepper|16|True|We kunnen hen elkaar niet zomaar uit laten moorden.
Pepper|17|False|Ik voel me verantwoordelijk om dit gevecht ten einde te brengen.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Maar hoe dan?
Arra|2|True|Precies? Hoe denk je dat te doen, jonge heks?
Arra|3|False|Ik voel dat nog niet veel van je Rea hersteld is.
Pepper|4|True|Je hebt helemaal gelijk, maar ik heb een spreuk in gedachte die alles op kan lossen.
Pepper|5|False|Het enige wat ik nodig heb om iedereen daar beneden te bereiken, is jouw Rea.
Arra|6|True|Energie geven aan een heks?
Arra|7|True|Ontoelaatbaar!
Arra|8|False|VERGEET HET MAAR!
Pepper|9|False|Heb je dan liever dat het slagveld een slachtveld wordt?
Torreya|10|True|Alsjeblieft, Arra. Voor deze éne keer? Die meiden en die draken die daar vechten, zijn onze School, onze familie.
Torreya|11|False|Ook die van jou.
Shichimi|12|False|Toe, Arra, alsjeblieft?
Arra|13|True|PFF! Goed dan!
Arra|14|False|Maar het is op eigen risico!

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|False|f s j j j j ! !
Pepper|2|False|WOOOOOOW!
Pepper|3|False|Dit is dus hoe Drakenrea aanvoelt!
Shichimi|4|False|Snel, Pepper! De tijd dringt!
Pepper|5|True|Deus... !
Pepper|6|True|Liefdus... !
Pepper|7|True|Kentus... !
Pepper|8|True|Geenus!
Pepper|9|False|...MAATUS !
Geluid|10|False|Dzzjooee! !

### P14

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|True|Fiizz!
Geluid|2|True|Dzjiing!
Geluid|3|True|Ffhii!
Geluid|4|True|Sjiii!
Geluid|5|True|Shsss!
Geluid|6|True|Fiizz!
Geluid|7|True|Dzjiing!
Geluid|8|True|Sjiii!
Geluid|9|True|Ffhii!
Geluid|10|False|Schsss!
Pepper|11|True|Dit was mijn eerste poging tot een anti-oorlogsspreuk.
Pepper|12|True|Deze verandert vijanden in vrienden, en
Pepper|13|False|geweld in liefde en compassie.
Shichimi|14|True|Waauuuw!
Shichimi|15|True|Maar... maar dat is geniaal Pepper!
Shichimi|16|False|Ze stoppen met vechten!
Torreya|17|False|Het werkt!

### P15

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Maar wat doen ze nu?
Shichimi|2|False|Sommigen zijn aan het zoenen...?!
Torreya|3|True|Uhm... dat zijn flink wat nieuwe stelletjes!
Torreya|4|False|Was dit je bedoeling,Pepper?
Pepper|5|False|Nee, niet echt! Ik geloof dat de Drakenrea de liefde in mijn spreuk wat sterker heeft gemaakt.
Torreya|6|False|Haha, dit gevecht zal zéker in de geschiedenisboeken terecht komen!
Shichimi|7|True|Hihi!
Shichimi|8|False|Ongetwijfeld!
Pepper|9|False|Oooh,wat gênant!
Geschrift|10|False|- EINDE -

### P16

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|1|False|15 december 2021 Tekeningen & verhaal: David Revoy. Bèta-feedback: Arlo James Barnes, Bhoren, Bobby Hiltz, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Karl Ove Hufthammer, Nicolas Artance, Pierre-Antoine Angelini, Valvin. Nederlandse versie Vertaling: Julien Bertholon, Marno van der Maas. Gebaseerd op het Hereva-universum Bedenker: David Revoy. Hoofdonderhouder: Craig Maloney. Schrijvers: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Verbeteraars: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 5.0beta, Inkscape 1.1 op Kubuntu Linux 20.04 Licentie: Creative Commons Naamsvermelding 4.0. www.peppercarrot.com
Pepper|2|False|Wist je dat?
Pepper|3|False|Pepper&Carrot is helemaal vrij,open-bron en gesponsord door de giften van haar lezers.
Pepper|4|False|Voor deze aflevering bedank ik 1036 patronen!
Pepper|5|False|Jij kan ook patroon van Pepper&Carrot worden en dan komt jouw naam hierbij!
Pepper|6|False|Je kan doneren met Patreon, Tipeee, PayPal, Liberapay ... en meer!
Pepper|7|False|Kijk op www.peppercarrot.com voor alle info!
Pepper|8|False|Dank je!
<hidden>|9|False|You can also translate this page if you want.
<hidden>|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
